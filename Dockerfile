FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /build/libs/maffia-0.0.1-SNAPSHOT.jar app.jar
RUN adduser -D myuser
USER myuser
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Xmx512m -jar","/app.jar"]
CMD java --server.port=$PORT -jar app.jar