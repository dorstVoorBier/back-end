package be.kdg.maffia.room.services;

import be.kdg.maffia.room.exceptions.RoomServiceException;
import be.kdg.maffia.room.model.Room;
import be.kdg.maffia.room.repositories.RoomRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RoomServiceImplTest {

    @Autowired
    private RoomService roomService;
    @MockBean
    private RoomRepository roomRepository;

    private ArrayList<String> playernames;
    private Room testRoom;
    @Before
    public void setUp() throws Exception {
        playernames = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            playernames.add("testUser"+i);
        }
        testRoom = new Room("testRoom",20,7,playernames,60,60,false,false,"","testUser0");
    }

    @Test
    public void testLeaveRoom(){
        when(roomRepository.findRoomById(any(String.class))).thenReturn(testRoom);
        int originalSize = testRoom.getPlayers().size();
        String playerToLeave = playernames.get(1);
        Room newRoom = roomService.leave("1",playerToLeave);
        Assert.assertNotEquals(newRoom.getPlayers().size(),originalSize);
        Assert.assertFalse(newRoom.getPlayers().contains(playerToLeave));
    }

    @Test
    public void testHostLeavesRoom(){
        when(roomRepository.findRoomById("1")).thenReturn(testRoom);
        roomService.leave("1",testRoom.getHost());
        Room room = roomService.getRoom("1");
        verify(roomRepository,times(1)).delete(testRoom);
    }
    @Test
    public void testUpdateSettings(){
        when(roomRepository.findRoomById("1")).thenReturn(testRoom);
        int originalValue = testRoom.getSecNightRoundTime();
        String newValue ="999999";
        Room room = roomService.updateSetting("1","night",newValue);
        verify(roomRepository,times(1)).save(room);
        Assert.assertNotEquals(room.getSecNightRoundTime(),originalValue);
        Assert.assertEquals(room.getSecNightRoundTime(),Integer.parseInt(newValue));
    }

    @Test(expected = RoomServiceException.class)
    public void testUpdateSettingsInvalid(){
        when(roomRepository.findRoomById("1")).thenReturn(testRoom);
        int originalValue = testRoom.getSecNightRoundTime();
        String newValue ="LETTERS NOT ALLOWED";
        Room room = roomService.updateSetting("1","night",newValue);
    }

}