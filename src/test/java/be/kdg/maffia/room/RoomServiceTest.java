package be.kdg.maffia.room;

import be.kdg.maffia.room.model.Room;
import be.kdg.maffia.room.services.RoomService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomServiceTest {

    @Autowired
    RoomService roomService;

    @Test
    public void testGetRoomsSortedAsc(){
        List<Room> rooms = roomService.getRoomsWithPlayersAsc();
        boolean sorted =true;
        Room previousRoom = null;
        for(Room room:rooms){
            if(previousRoom==null)previousRoom=room;
            else{
                int compareResult = previousRoom.getRoomName().compareToIgnoreCase(room.getRoomName());
                if(compareResult>0)sorted=false;
            }
        }
        Assert.assertTrue(sorted);
    }

    @Test
    public void testGetRoomsSortedDesc(){
        List<Room> rooms = roomService.getRoomsWithPlayersDesc();
        boolean sorted = true;
        Room previousRoom =null;
        for (Room room:rooms){
            if(previousRoom==null)previousRoom=room;
            else{
                int compareResult = previousRoom.getRoomName().compareToIgnoreCase(room.getRoomName());
                if(compareResult<0)sorted=false;
            }
        }
        Assert.assertTrue(sorted);
    }
}
