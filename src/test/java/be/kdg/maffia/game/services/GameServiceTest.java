package be.kdg.maffia.game.services;

import be.kdg.maffia.game.model.*;
import be.kdg.maffia.game.model.playerroles.*;
import be.kdg.maffia.game.model.votes.Vote;
import be.kdg.maffia.game.repositories.GameRepository;
import be.kdg.maffia.game.repositories.PlayerRepository;
import be.kdg.maffia.game.repositories.PlayerRoleRepository;
import be.kdg.maffia.game.services.interfaces.GameService;
import be.kdg.maffia.game.services.interfaces.PlayerService;
import be.kdg.maffia.game.services.interfaces.RoundService;
import be.kdg.maffia.login.model.ApplicationUser;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GameServiceTest {

    @MockBean
    PlayerRoleRepository playerRoleRepository;
    @MockBean
    PlayerRepository playerRepository;
    @MockBean
    GameRepository gameRepository;

    @MockBean
    ProfileService profileService;
    @Autowired
    PlayerService playerService;

    private List<Player> roomWith7 = new ArrayList<>();
    private List<Player> roomWith11 = new ArrayList<>();
    private List<Player> roomWith14 = new ArrayList<>();
    private List<Player> roomWith18 = new ArrayList<>();

    private List<Player> maffiaList = new ArrayList<>();
    private List<Player> citizenList = new ArrayList<>();

    private List<String> usernamesRoom7 = new ArrayList<>();
    private List<String> usernamesRoom11 = new ArrayList<>();
    private List<String> usernamesRoom14 = new ArrayList<>();
    private List<String> usernamesRoom18 = new ArrayList<>();


    private Game gameMock7;
    private Game gameMock11;
    private Game gameMock14;
    private Game gameMock18;


    @Autowired
    private GameService gameService;

    @Autowired
    private RoundService roundService;

    @Before
    public void setup() {
        Maffia maffia = new Maffia();
        Citizen citizen = new Citizen();
        Doctor doctor = new Doctor();
        Sherrif sherrif = new Sherrif();

        when(playerRoleRepository.getByRolename("maffia")).thenReturn(maffia);
        when(playerRoleRepository.getByRolename("citizen")).thenReturn(citizen);
        when(playerRoleRepository.getByRolename("doctor")).thenReturn(doctor);
        when(playerRoleRepository.getByRolename("sherrif")).thenReturn(sherrif);

        PlayerState defaultstate = new PlayerState();
        for (int i = 0; i < 18; i++) {
            Player mockPlayer = new Player();
            String username = "testUser"+i;
            mockPlayer.setUsername(username);
            mockPlayer.setPlayerId(Integer.toString(i));
            usernamesRoom18.add(username);
            if(i<14){
                usernamesRoom14.add(username);
            }
            if(i<11){
                usernamesRoom11.add(username);
            }
            if(i<7){
                usernamesRoom7.add(username);
            }
        }
        for (Player maffiaPlayer : maffiaList) {
            if (roomWith7.size() < 2) {
                usernamesRoom7.add(maffiaPlayer.getUsername());
                roomWith7.add(maffiaPlayer);
            }
            if (roomWith11.size() < 3) {
                usernamesRoom11.add(maffiaPlayer.getUsername());
                roomWith11.add(maffiaPlayer);
            }
            if (roomWith14.size() < 4) {
                usernamesRoom14.add(maffiaPlayer.getUsername());
                roomWith14.add(maffiaPlayer);
            }
            if (roomWith18.size() < 5) {
                usernamesRoom18.add(maffiaPlayer.getUsername());
                roomWith18.add(maffiaPlayer);
            }
        }
        for (Player citizenPlayer : citizenList) {
            if (roomWith7.size() != 7) {
                usernamesRoom7.add(citizenPlayer.getUsername());
                roomWith7.add(citizenPlayer);
            }
            if (roomWith11.size() != 11) {
                usernamesRoom11.add(citizenPlayer.getUsername());
                roomWith11.add(citizenPlayer);
            }
            if (roomWith14.size() != 14) {
                usernamesRoom14.add(citizenPlayer.getUsername());
                roomWith14.add(citizenPlayer);
            }
            if (roomWith18.size() != 18) {
                usernamesRoom18.add(citizenPlayer.getUsername());
                roomWith18.add(citizenPlayer);
            }
        }

    }

    @Test
    public void createNewGameContainsAllPlayers() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game testGame = gameService.startNewGame(usernamesRoom7);
        Assert.assertThat(testGame.getAvailablePlayers().size(), equalTo(usernamesRoom7.size()));
    }

    //Test with 7 players
    @Test
    public void createNewGameContains2Maffias() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game testGame = gameService.startNewGame(usernamesRoom7);
        Assert.assertThat(testGame.getMaffias().size(), equalTo(2));
    }

    //Test with 9 players
    @Test
    public void createNewGameContains3Maffias() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock11);
        Game testGame = gameService.startNewGame(usernamesRoom11);
        int maffiaCount = 0;
        Assert.assertThat(testGame.getMaffias().size(), equalTo(3));
    }

    //Test with 9 players
    @Test
    public void createNewGameContains4Maffias() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock14);
        Game testGame = gameService.startNewGame(usernamesRoom14);
        int maffiaCount = 0;
        Assert.assertThat(testGame.getMaffias().size(), equalTo(4));
    }

    //Test with 9 players
    @Test
    public void createNewGameContains5Maffias() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock18);
        Game testGame = gameService.startNewGame(usernamesRoom18);
        int maffiaCount = 0;
        Assert.assertThat(testGame.getMaffias().size(), equalTo(5));
    }

    @Test
    public void createNewGameAllPlayersHaveRoles() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game game = gameService.startNewGame(usernamesRoom11);
        boolean emptyRoles = false;
        for (Player p : game.getCitizens()) {
            if (!p.getRole().getClass().getSuperclass().getName().equals(PlayerRole.class.getName())) emptyRoles = true;
        }
        assertFalse(emptyRoles);
    }

    @Test
    public void startFirstRoundCheckPlayers() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game game = gameService.startNewGame(usernamesRoom7);
        roundService.startNewRound(game);
        assertThat(game.getAvailablePlayers(), equalTo(game.getCurrentRound().getAvailablePlayers()));
    }

    @Test
    public void endRoundWithDeadPlayer() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        int size = usernamesRoom7.size();
        Game game = gameService.startNewGame(usernamesRoom7);
        game.setGameId("1");
        when(gameRepository.findByGameId("1")).thenReturn(game);
        roundService.addVote(game, game.getMaffias().get(0).getUsername(), Arrays.asList(game.getCurrentRound().getAvailablePlayers().get(0).getUsername()));
        Game gameWithDeadPlayer = roundService.endRound(game);
        assertNotEquals(gameWithDeadPlayer.getCitizens(), size);
        assertThat(gameWithDeadPlayer.getDeadPlayers().size(), equalTo(1));
    }

    @Test
    public void startNewRoundCheckDeadPlayers() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game game = gameService.startNewGame(usernamesRoom7);
        when(gameRepository.findByGameId("1")).thenReturn(gameMock7);
        Player playerToKill = game.getCurrentRound().getAvailablePlayers().get(2);
        //The first round of a Game is always Night. Only Maffias can add votes during the night.
        Vote vote = roundService.addVote(game, game.getMaffias().get(0).getUsername(), Arrays.asList(playerToKill.getUsername()));
        Game testGameAfterRound1 = new Game(roomWith7);
        Game gameWithDeadPlayer = roundService.endRound(game);
        when(gameRepository.findByGameId("1")).thenReturn(testGameAfterRound1);
        Assert.assertThat(gameWithDeadPlayer.getCurrentRound().getAvailablePlayers().size(), equalTo(6));
        assertFalse(gameWithDeadPlayer.getCurrentRound().getAvailablePlayers().contains(playerToKill));
        assertTrue(gameWithDeadPlayer.getDeadPlayers().contains(playerToKill));
    }

    @Test
    public void endMaffiaVotingResultsInKill() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game game = gameService.startNewGame(usernamesRoom7);
        game.setGameId("1");
        Round testRound = game.getCurrentRound();
        when(gameRepository.findByGameId("1")).thenReturn(game);
        Player playerToKill = testRound.getAvailablePlayers().get(2);
        roundService.addVote(game, game.getMaffias().get(0).getUsername(), Arrays.asList(playerToKill.getUsername()));
        Game gameWithDeadPlayer = roundService.endRound(game);
        Assert.assertThat(gameWithDeadPlayer.getDeadPlayers().size(), equalTo(1));
        Assert.assertThat(gameWithDeadPlayer.getDeadPlayers().get(0).getUsername(), equalTo(playerToKill.getUsername()));
    }

    @Test
    public void testKillProtectedPlayer() {
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game game = gameService.startNewGame(usernamesRoom7);
        game.setGameId("1");

        Player doctor=null;
        for(Player p:game.getCitizens()){
            if(p.getRole().getRolename().equals("doctor"))doctor=p;
        }
        assertNotNull("No doctor in game!",doctor);
        roundService.addVote(game, doctor.getUsername(), Arrays.asList("testUser1"));
        roundService.addVote(game, game.getMaffias().get(0).getUsername(), Arrays.asList("testUser1"));
        assertThat(game.getCurrentRound().getClass(),equalTo(RoundNight.class));
        RoundNight currentRound = (RoundNight)game.getCurrentRound();
        for(Vote vote: currentRound.getPowerVotes()){
            roundService.handlePowerVote(game, vote);
        }
        Game gameWhenRoundEnds = roundService.endRound(game);
        Assert.assertThat(gameWhenRoundEnds.getAvailablePlayers().size(), equalTo(7));
    }

    @Test
    public void testEndRoundWithNoMaffiaVotes(){
        when(gameRepository.save(any(Game.class))).thenReturn(gameMock7);
        Game game = gameService.startNewGame(usernamesRoom7);
        game.setGameId("1");

        Player doctor=null;
        for(Player p:game.getCitizens()){
            if(p.getRole().getRolename().equals("doctor"))doctor=p;
        }
        assertNotNull("No doctor in game!",doctor);
        roundService.addVote(game, doctor.getUsername(), Arrays.asList("testUser1"));
        roundService.addVote(game, doctor.getUsername(), Arrays.asList("testUser1"));
        assertThat(game.getCurrentRound().getClass(),equalTo(RoundNight.class));
        RoundNight currentRound = (RoundNight)game.getCurrentRound();
        for(Vote vote: currentRound.getPowerVotes()){
            roundService.handlePowerVote(game, vote);
        }
        Game gameWhenRoundEnds = roundService.endRound(game);
        Assert.assertThat(gameWhenRoundEnds.getAvailablePlayers().size(), equalTo(7));
    }

    @Test
    public void testDoctorProtectsHimself() {
        Game game = gameService.startNewGame(usernamesRoom7);
        Player doctor = null;
        Player maffia = null;
        for (Player p : game.getCitizens()) {
            if (p.getRole().getRolename().equals("doctor")) doctor = p;
        }
        assertNotNull("No doctor in game!", doctor);
        roundService.addVote(game, doctor.getUsername(), Arrays.asList(doctor.getUsername()));
        roundService.addVote(game, game.getMaffias().get(0).getUsername(), Arrays.asList(doctor.getUsername()));
        RoundNight currentRound = (RoundNight)game.getCurrentRound();
        for(Vote vote:currentRound.getPowerVotes()){
            roundService.handlePowerVote(game,vote);
        }
        Game gameWhenRoundEnds = roundService.endRound(game);
        Assert.assertTrue(gameWhenRoundEnds.getDeadPlayers().isEmpty());
    }
}