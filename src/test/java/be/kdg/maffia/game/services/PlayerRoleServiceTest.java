package be.kdg.maffia.game.services;

import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.game.model.playerroles.*;
import be.kdg.maffia.game.repositories.PlayerRoleRepository;
import be.kdg.maffia.game.services.interfaces.PlayerRoleService;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PlayerRoleServiceTest {
    private List<Player> playersSize7 = new ArrayList<>();
    private List<Player> playersSize11 = new ArrayList<>();
    private List<Player> playersSize14 = new ArrayList<>();
    private List<Player> playersSize18 = new ArrayList<>();

    @Autowired
    private PlayerRoleService roleService;

    @MockBean
    private PlayerRoleRepository roleRepository;

    @MockBean
    private ProfileService profileService;

    @Before
    public void setup() {

        when(roleRepository.getByRolename("citizen")).thenReturn(new Citizen());
        when(roleRepository.getByRolename("maffia")).thenReturn(new Maffia());
        when(roleRepository.getByRolename("sherrif")).thenReturn(new Sherrif());
        when(roleRepository.getByRolename("doctor")).thenReturn(new Doctor());
        for (int i = 0; i < 18; i++) {
            String username = "testUser" + i;
            if (i < 7) {
                playersSize7.add(new Player(username));
            }
            if (i < 11) {
                playersSize11.add(new Player(username));
            }
            if (i < 14) {
                playersSize14.add(new Player(username));
            }
            playersSize18.add(new Player(username));
        }
    }

    @Test
    public void testSetRolesWith7() {
        List<Player> playersWithRoles = roleService.setRoles(playersSize7);
        Assert.assertNotNull(playersWithRoles);
        Assert.assertFalse(playersWithRoles.isEmpty());
        int maffiaCount = 0;
        int citizenCount = 0;
        int sherrifCount = 0;
        int doctorCount = 0;
        Assert.assertThat(playersWithRoles.size(), equalTo(7));

        for (Player p : playersWithRoles) {
            switch (p.getRole().getRolename()) {
                case "citizen":
                    citizenCount++;
                    break;
                case "maffia":
                    maffiaCount++;
                    break;
                case "sherrif":
                    sherrifCount++;
                    break;
                case "doctor":
                    doctorCount++;
                    break;
            }
        }
        Assert.assertThat("Expected amount of maffias is 2", maffiaCount, equalTo(2));
        Assert.assertThat("Expected amount of citizens is 3",citizenCount, equalTo(3));
        Assert.assertThat("E",sherrifCount, equalTo(1));
        Assert.assertThat(doctorCount, equalTo(1));
    }

    @Test
    public void testSetRolesWith11() {
        List<Player> playersWithRoles = roleService.setRoles(playersSize11);
        Assert.assertNotNull(playersWithRoles);
        Assert.assertFalse(playersWithRoles.isEmpty());
        Map<String, Integer> roleCount = countRoles(playersWithRoles);

        Assert.assertThat("Expected amount of maffias is 3", roleCount.get("maffia"), equalTo(3));
        Assert.assertThat("Expected amount of citizens is 6",roleCount.get("citizen"), equalTo(6));
        Assert.assertThat("Expected amount of sherrifs is 1",roleCount.get("sherrif"), equalTo(1));
        Assert.assertThat("Expected amount of doctors is 1",roleCount.get("doctor"), equalTo(1));
    }
    private Map<String,Integer> countRoles(List<Player> players){
        Map<String ,Integer> countMap = new HashMap<>();
        for (Player p : players) {
            countMap.merge(p.getRole().getRolename(), 1, (a, b) -> a + b);
        }
        return countMap;
    }
    public void testGetPlayerRole(){
        String roleToFetch="citizen";
        PlayerRole role = roleService.getRoleByName(roleToFetch);
        Assert.assertNotNull(role);
        Assert.assertThat(role.getRolename(),equalTo(roleToFetch));
        verify(roleRepository,times(1)).getByRolename(roleToFetch);
    }
}
