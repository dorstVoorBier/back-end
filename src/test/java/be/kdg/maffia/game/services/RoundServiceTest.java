package be.kdg.maffia.game.services;

import be.kdg.maffia.game.model.*;
import be.kdg.maffia.game.model.playerroles.Citizen;
import be.kdg.maffia.game.model.playerroles.Doctor;
import be.kdg.maffia.game.model.playerroles.Maffia;
import be.kdg.maffia.game.model.votes.Vote;
import be.kdg.maffia.game.repositories.GameRepository;
import be.kdg.maffia.game.repositories.PlayerRepository;
import be.kdg.maffia.game.services.interfaces.RoundService;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)

public class RoundServiceTest {
    private Game testGame;
    private Round testRound;

    @MockBean
    PlayerRepository playerRepository;
    @MockBean
    GameRepository gameRepository;
    @MockBean
    ProfileService profileService;

    @Autowired
    private RoundService roundService;

    @Before
    public void setup() {

        List<Player> players5 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Player newPlayer = new Player("testUser"+i);
            newPlayer.setPlayerId(Integer.toString(i));
            players5.add(newPlayer);
            when(playerRepository.findByPlayerId(Integer.toString(i))).thenReturn(newPlayer);
        }
        for (int i = 0; i < 2; i++) {
            players5.get(i).setRole(new Maffia());
        }
        for (int i = 2; i < players5.size(); i++) {
            players5.get(i).setRole(new Citizen());
        }
        for (Player p : players5) {
            when(playerRepository.findByUsername(p.getUsername())).thenReturn(p);
        }
        testGame = new Game(players5);
        testGame.setDay(false);
        testRound = new RoundDay("1",players5);
        testGame.setCurrentRound(testRound);
    }


    @Test
    public void addStemTest() {
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        Player voted = testRound.getAvailablePlayers().get(2);
        roundService.addVote(testGame,"testUser0",Arrays.asList(voted.getUsername()));
        Assert.assertThat(testGame.getCurrentRound().getVotes().size(), equalTo(1));
        Assert.assertThat(testGame.getCurrentRound().getVotes().get(0).getVoted().get(0).getUsername(), equalTo(voted.getUsername()));
    }
}
