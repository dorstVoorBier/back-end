package be.kdg.maffia.rules.services.implementations;

import be.kdg.maffia.rules.model.InfoFile;
import be.kdg.maffia.rules.repositories.interfaces.InfoRepository;
import be.kdg.maffia.rules.services.exceptions.InfoServiceException;
import be.kdg.maffia.rules.services.exceptions.InvalidRoleException;
import be.kdg.maffia.rules.services.interfaces.InfoService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InfoServiceImplTest {

    @Value("${rules.filename}")
    private String filenameRules;
    @Value("${citizen.filename}")
    private String filenameCitizen;
    @Value("${doctor.filename}")
    private String filenameDoctor;
    @Value("${maffia.filename}")
    private String filenameMaffia;
    @Value("${sherrif.filename}")
    private String filenameSherrif;

    @MockBean
    private InfoRepository infoRepository;
    @Autowired
    private InfoService infoService;

    @Before
    public void setup(){
        when(infoRepository.getByFilename(filenameRules)).thenReturn(new InfoFile(filenameRules,"Testcontent",11));
        when(infoRepository.getByFilename(filenameCitizen)).thenReturn(new InfoFile(filenameCitizen,"Testcontent",11));
        when(infoRepository.getByFilename(filenameMaffia)).thenReturn(new InfoFile(filenameMaffia,"Testcontent",11));
        when(infoRepository.getByFilename(filenameDoctor)).thenReturn(new InfoFile(filenameDoctor,"Testcontent",11));
        when(infoRepository.getByFilename(filenameSherrif)).thenReturn(new InfoFile(filenameSherrif,"Testcontent",11));
    }

    @Test
    public void testGetRules() throws Exception{
        InfoFile infoFile = infoService.getRules();
        Assert.assertNotNull(infoFile);
        Assert.assertThat(infoFile.getFilename(),equalTo(filenameRules));
        verify(infoRepository,times(1)).getByFilename(filenameRules);
    }
    @Test
    public void testGetInfoFileCitizen() throws Exception {
        InfoFile citizen = infoService.getRoleInfo("citizen");
        Assert.assertNotNull("No file found for role: citizen",citizen);
        Assert.assertThat("Filename does not match: citzen",citizen.getFilename(),equalTo(filenameCitizen));
        verify(infoRepository,times(1)).getByFilename(filenameCitizen);
    }

    @Test
    public void testGetInfoFileMaffia() throws Exception {
        InfoFile maffia = infoService.getRoleInfo("maffia");
        Assert.assertNotNull("No file found for role: maffia",maffia);
        Assert.assertThat("Filename does not match: maffia",maffia.getFilename(),equalTo(filenameMaffia));
        verify(infoRepository,times(1)).getByFilename(filenameMaffia);
    }
    @Test
    public void testGetInfoFileDoctor() throws Exception{
        InfoFile doctor = infoService.getRoleInfo("doctor");
        Assert.assertNotNull("No file found for role: doctor",doctor);
        Assert.assertThat("Filename does not match: doctor",doctor.getFilename(),equalTo(filenameDoctor));
        verify(infoRepository,times(1)).getByFilename(filenameDoctor);
    }
    @Test
    public void testGetInfoFileSherrif() throws Exception{
        InfoFile sherrif = infoService.getRoleInfo("sherrif");
        Assert.assertNotNull("No file found for role: sherrif",sherrif);
        Assert.assertThat("Filename does not match: sherrif",sherrif.getFilename(),equalTo(filenameSherrif));
        verify(infoRepository,times(1)).getByFilename(filenameSherrif);
    }

    @Test(expected = InvalidRoleException.class)
    public void testGetInfoFileInvalid() throws Exception{
        infoService.getRoleInfo("undefined");
    }
}