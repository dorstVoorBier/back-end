package be.kdg.maffia.chat.services;

import be.kdg.maffia.chat.model.ChatMessage;
import be.kdg.maffia.chat.repositories.ChatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatServiceImplTest {
    @Autowired
    private ChatService chatService;
    @MockBean
    private ChatRepository chatRepository;

    @Test
    public void testAddMessage(){
        chatService.addMessage(new ChatMessage("TestMessage","testUser"),"1");
        verify(chatRepository,times(1)).findByRoomId("1");
    }
}