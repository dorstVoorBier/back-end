package be.kdg.maffia.integration;

import be.kdg.maffia.game.controller.GameController;
import be.kdg.maffia.game.controller.RoundController;
import be.kdg.maffia.game.model.*;
import be.kdg.maffia.game.model.dto.*;
import be.kdg.maffia.game.model.playerroles.Citizen;
import be.kdg.maffia.game.model.playerroles.Doctor;
import be.kdg.maffia.game.model.playerroles.Maffia;
import be.kdg.maffia.game.model.playerroles.Sherrif;
import be.kdg.maffia.game.repositories.GameRepository;
import be.kdg.maffia.game.repositories.PlayerRepository;
import be.kdg.maffia.game.repositories.PlayerRoleRepository;
import be.kdg.maffia.game.services.interfaces.PlayerRoleService;
import be.kdg.maffia.game.services.interfaces.PlayerService;
import be.kdg.maffia.login.services.ApplicationUserService;
import be.kdg.maffia.room.model.Room;
import be.kdg.maffia.room.repositories.RoomRepository;
import be.kdg.maffia.room.services.RoomService;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.internal.matchers.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.constraints.AssertTrue;
import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class PlayGameTest {
    private Game testGame;
    private Round testRound1;
    private Round testRound2;
    private Round testRound3;
    private Room testRoom;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ApplicationUserService applicationUserService;

    @Autowired
    private GameController gameController;
    @Autowired
    private RoundController roundController;

    @Autowired
    private PlayerService playerService;

    @MockBean
    private GameRepository gameRepository;
    @MockBean
    private PlayerRepository playerRepository;
    @MockBean
    private PlayerRoleRepository roleRepository;
    @MockBean
    private PlayerRoleService roleService;
    @MockBean
    private RoomService roomService;
    @MockBean
    private ProfileService profileSerivce;


    private List<Player> players = new ArrayList<>();
    private List<Player> playersWithRoles = new ArrayList<>();
    private List<String> usernames = new ArrayList<>();
    private String roomId = "20";

    @Before
    public void setup() {
        Game game = new Game();
        List<Player> citizens = new ArrayList<>();
        List<Player> maffias = new ArrayList<>();

        Maffia maffia = new Maffia();
        Citizen citizen = new Citizen();
        Doctor doctor = new Doctor();
        Sherrif sherrif = new Sherrif();

        when(roleService.getRoleByName("maffia")).thenReturn(maffia);
        when(roleService.getRoleByName("citizen")).thenReturn(citizen);
        when(roleService.getRoleByName("doctor")).thenReturn(doctor);
        when(roleService.getRoleByName("sherrif")).thenReturn(sherrif);

        //Create mocks for players
        for (int i = 0; i < 7; i++) {
            testRoom = mock(Room.class);
            when(testRoom.getPlayers()).thenReturn(usernames);
            when(testRoom.getId()).thenReturn(roomId);
            when(roomService.getRoom(roomId)).thenReturn(testRoom);
            Player p = new Player();
            String username = "testUser"+i;
            p.setPlayerId(Integer.toString(i));
            p.setUsername(username);
            usernames.add(username);
            if (i < 2) {
                p.setRole(maffia);
                playersWithRoles.add(p);
                maffias.add(p);
            } else if (i == 2) {
                p.setRole(doctor);
                playersWithRoles.add(p);
                citizens.add(p);
            } else if (i == 3) {
                p.setRole(sherrif);
                playersWithRoles.add(p);
                citizens.add(p);
            } else {
                p.setRole(citizen);
                playersWithRoles.add(p);
                citizens.add(p);
            }
        }

        Room room = new Room();
        room.setId(roomId);
        room.setPlayers(usernames);

        testGame = new Game();
        when(roleService.setRoles(Matchers.anyList())).thenReturn(playersWithRoles);
        testGame.setCitizens(citizens);
        testGame.setMaffias(maffias);
        testGame.setGameId("1");
        testGame.setDeadPlayers(new ArrayList<>());
        testGame.setRounds(new ArrayList<>());

        testRound1 = new RoundNight("1",testGame.getAvailablePlayers());
        testGame.setCurrentRound(testRound1);
//        testGame.setAllRoles(Arrays.asList(maffia, citizen, sherrif, doctor));
//        testGame.setCitizens(citizensWithDoctor);
//        testGame.setMaffias(maffias);
//        testGame.setDeadPlayers(new ArrayList<>());
//        testGame.setRounds(new ArrayList<>());
//        testRound1 = new RoundNight("1", testGame.getAvailablePlayers());
//        testRound1.setRoundId("1");
//        testRound1.setRoundOfGame(1);
//        testGame.setCurrentRound(testRound1);
    }

    @Test
    public void startGame() {
        when(gameRepository.save(any(Game.class))).thenReturn(testGame);
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        ResponseEntity result = gameController.startGame(roomId);
        assert result.getStatusCode().equals(HttpStatus.OK);
        assert !result.getBody().getClass().equals(String.class);
        GameDto dto = (GameDto) result.getBody();
        dto.setGameId("1");
        assert dto != null;
        Assert.assertThat(dto.getPlayers().size(), equalTo(7));
        Assert.assertThat(dto.getDeadPlayers().size(), equalTo(0));
    }

    @Test
    public void startNewRound() {
        when(gameRepository.save(any(Game.class))).thenReturn(testGame);
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        ResponseEntity gameResponse = gameController.startGame(roomId);
        Assert.assertThat(gameResponse.getBody().getClass(), equalTo(GameDto.class));
        RoundDto roundDto = (RoundDto) roundController.getCurrentRound("1").getBody();
        assert roundDto != null;
        Assert.assertThat(roundDto.getAvailablePlayers().size(), equalTo(7));
    }

    /**
     * Test if votes are handled correctly with the right response (a dead player)
     */
    @Test
    public void voteAndSetReady() {
        when(gameRepository.save(any(Game.class))).thenReturn(testGame);
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        GameDto game = (GameDto) gameController.startGame(roomId).getBody();
        game.setGameId("1");
        game.setCurrentRound("1");
        assert game != null;
        String votedUsername = "testUser1";
        RoundDto round1 = (RoundDto) roundController.getCurrentRound("1").getBody();
        assert round1 != null;
        Assert.assertThat(round1.getClass(), equalTo(RoundNightDto.class));
        GameDto votingResult = null;
        int amountReady = 0;
        for (String player: round1.getAvailablePlayers()) {
            ResponseEntity res = gameController.getPlayer(game.getGameId(), player);
            if(res.getBody().getClass().equals(PlayerDto.class)){
                PlayerDto resDto = (PlayerDto)res.getBody();
                if(resDto.getRolename().equals("citizen")){
                    amountReady++;
                }
            }
        }
        for (int i = 0; i < round1.getAvailablePlayers().size(); i++) {
            ResponseEntity playerRes = gameController.getPlayer(game.getGameId(), game.getPlayers().get(i));
            PlayerDto playerDto = (PlayerDto)playerRes.getBody();
            if(!playerDto.getRolename().equals("citizen")){
                if (!game.getPlayers().get(i).equals("testUser2")) {
                    VoteDto voteDto = new VoteDto();
                    voteDto.setVoter(game.getPlayers().get(i));
                    voteDto.setVoted(Arrays.asList(votedUsername));
                    roundController.vote("1", voteDto);
                }
                ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
                if (result.getBody().getClass().equals(Integer.class)) {
                    amountReady++;
                    Assert.assertThat(amountReady, equalTo(round1.getAvailablePlayers().size() - (Integer) result.getBody()));
                }
                if (result.getBody().getClass().equals(GameDto.class)) {
                    votingResult = (GameDto) result.getBody();
                    Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                }
            }
        }
        assert votingResult != null;
        Assert.assertThat(votingResult.getPlayers().size(), equalTo(6));
        Assert.assertThat(votingResult.getDeadPlayers().size(), equalTo(1));
        Assert.assertThat(votingResult.getDeadPlayers().get(votedUsername), notNullValue());
        Assert.assertTrue(votingResult.getDeadPlayers().containsKey(votedUsername));
        System.out.println();
    }

    /**
     * Test that starts a new game and plays 2 rounds (1 day, 1 night)
     * to test if dayVotes and Nightvotes are handled correctly after eachother
     */
    @Test
    public void PlayGameDayAndNight() {
        //First Round Night
        when(gameRepository.save(any(Game.class))).thenReturn(testGame);
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        GameDto game = (GameDto) gameController.startGame(roomId).getBody();
        game.setGameId("1");
        game.setCurrentRound("1");
        assert game != null;
        String votedUsername = "testUser1";
        RoundDto round1 = (RoundDto) roundController.getCurrentRound(game.getGameId()).getBody();
        assert round1 != null;
        Assert.assertThat(round1.getClass(), equalTo(RoundNightDto.class));
        GameDto votingResultNight = null;
        int amountReady = 0;
        for (String player: round1.getAvailablePlayers()) {
            ResponseEntity res = gameController.getPlayer(game.getGameId(), player);
            if(res.getBody().getClass().equals(PlayerDto.class)){
                PlayerDto resDto = (PlayerDto)res.getBody();
                if(resDto.getRolename().equals("citizen")){
                    amountReady++;
                }
            }
        }
        for (int i = 0; i < round1.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            ResponseEntity playerRes = gameController.getPlayer(game.getGameId(), game.getPlayers().get(i));
            PlayerDto playerDto = (PlayerDto)playerRes.getBody();
            if(!playerDto.getRolename().equals("citizen")){
                if (!game.getPlayers().get(i).equals("testUser2")) {
                    voteDto.setVoter(game.getPlayers().get(i));
                    voteDto.setVoted(Arrays.asList(votedUsername));
                    roundController.vote(game.getGameId(), voteDto);
                }
                ResponseEntity result = roundController.ready(game.getGameId(), game.getPlayers().get(i));
                if (result.getBody().getClass().equals(Integer.class)) {
                    amountReady++;
                    Assert.assertThat(amountReady, equalTo(round1.getAvailablePlayers().size() - (Integer) result.getBody()));
                }
                if (result.getBody().getClass().equals(GameDto.class)) {
                    votingResultNight = (GameDto) result.getBody();
                    Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                }
            }
        }
        assert votingResultNight != null;
        votingResultNight.setCurrentRound("2");
        Assert.assertThat(votingResultNight.getPlayers().size(), equalTo(6));
        Assert.assertThat(votingResultNight.getDeadPlayers().size(), equalTo(1));
        Assert.assertThat(votingResultNight.getDeadPlayers().get(votedUsername), notNullValue());
        Assert.assertTrue(votingResultNight.getDeadPlayers().containsKey(votedUsername));
        game = votingResultNight;
        List<Player> remainingPlayers = new ArrayList<>();
        for (String player : votingResultNight.getPlayers()) {
            for (Player defaultPlayer : players) {
                if (defaultPlayer.getUsername().equals(player)) remainingPlayers.add(defaultPlayer);
            }
        }
        game.setCurrentRound("2");
        testRound2 = new RoundDay("2", remainingPlayers);
        testRound2.setRoundOfGame(2);
        //First Round Day
        RoundDto round2 = (RoundDto) roundController.getCurrentRound("1").getBody();
        assert round2 != null;
        Assert.assertThat(round2.getClass(), equalTo(RoundDayDto.class));
        GameDto votingResultDay = null;
        amountReady = 0;
        for (int i = 0; i < round2.getAvailablePlayers().size(); i++) {
            if (!game.getPlayers().get(i).equals("testUser2")) {
                VoteDto voteDto = new VoteDto();
                voteDto.setVoter(game.getPlayers().get(i));
                voteDto.setVoted(Arrays.asList("testUser0"));
                roundController.vote("1", voteDto);
            }
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(Integer.class)) {
                amountReady++;
                Assert.assertThat(amountReady, equalTo(round2.getAvailablePlayers().size() - (Integer) result.getBody()));
            }
            if (result.getBody().getClass().equals(GameDto.class)) {
                votingResultDay = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
            }
        }
        GameDto gameResult2;
        Assert.assertThat(votingResultDay.getPlayers().size(), equalTo(5));
        Assert.assertThat(votingResultDay.getDeadPlayers().size(), equalTo(2));
    }

    @Test
    public void testDoctorSavedVoted() {
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        GameDto game = (GameDto) gameController.startGame(roomId).getBody();
        game.setGameId("1");
        game.setCurrentRound("1");
        RoundDto round1 = (RoundDto) roundController.getCurrentRound("1").getBody();
        GameDto resultRound1 = null;
        for (int i = 0; i < round1.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(round1.getAvailablePlayers().get(i));
            voteDto.setVoted(Arrays.asList("testUser3"));
            roundController.vote(game.getGameId(), voteDto);
            ResponseEntity readyResult = roundController.ready(game.getGameId(), round1.getAvailablePlayers().get(i));
            Assert.assertThat(readyResult.getStatusCode(), equalTo(HttpStatus.OK));
            if (readyResult.getBody().getClass().equals(GameDto.class)) resultRound1 = (GameDto) readyResult.getBody();
        }
        Assert.assertThat(resultRound1.getPlayers().size(), equalTo(7));
        Assert.assertThat(resultRound1.getDeadPlayers().size(), equalTo(0));
    }

    @Test
    public void testEndGameCitizensWin() {
        when(gameRepository.save(any(Game.class))).thenReturn(testGame);
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        GameDto game = (GameDto) gameController.startGame(roomId).getBody();
        RoundDto round1 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round1.getRoundType(), equalTo("roundNight"));
        GameDto resultRound1 = null;
        for (int i = 0; i < round1.getAvailablePlayers().size(); i++) {
            if (!game.getPlayers().get(i).equals("testUser2")) {
                VoteDto voteDto = new VoteDto();
                voteDto.setVoter(game.getPlayers().get(i));
                voteDto.setVoted(Arrays.asList("testUser0"));
                roundController.vote("1", voteDto);
            }
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound1 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                game = resultRound1;
            }
        }
        Assert.assertThat(resultRound1.getDeadPlayers().size(), equalTo(1));
        RoundDto round2 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round2.getRoundType(), equalTo("roundDay"));
        GameDto resultRound2 = null;
        for (int i = 0; i < round2.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(game.getPlayers().get(i));
            voteDto.setVoted(Arrays.asList("testUser1"));
            roundController.vote("1", voteDto);
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound2 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
            }
        }
        Assert.assertNotEquals(null, resultRound2.getWinner());
        Assert.assertThat(resultRound2.getWinner(), equalTo("citizen"));
    }

    @Test
    public void testEndGameMaffiaWins() {
        when(gameRepository.save(any(Game.class))).thenReturn(testGame);
        when(gameRepository.findByGameId("1")).thenReturn(testGame);
        GameDto game = (GameDto) gameController.startGame(roomId).getBody();
        game.setGameId("1");
        game.setCurrentRound("1");
        int index = 0;
        RoundDto round1 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round1.getRoundType(), equalTo("roundNight"));
        GameDto resultRound1;
        for (int i = 0; i < round1.getAvailablePlayers().size(); i++) {
            if (!game.getPlayers().get(i).equals("testUser2")) {
                VoteDto voteDto = new VoteDto();
                voteDto.setVoter(game.getPlayers().get(i));
                voteDto.setVoted(Arrays.asList("testUser2"));
                roundController.vote("1", voteDto);
            }
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound1 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                game = resultRound1;
            }
        }

        RoundDto round2 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round2.getRoundType(), equalTo("roundDay"));
        GameDto resultRound2 = null;
        for (int i = 0; i < round2.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(game.getPlayers().get(i));
            voteDto.setVoted(Collections.singletonList("testUser3"));
            roundController.vote("1", voteDto);
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound2 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                game = resultRound2;
            }
        }
        game.setCurrentRound("3");
        List<Player> remainingPlayers3 = new ArrayList<>();
        for (String player : resultRound2.getPlayers()) {
            for (Player defaultPlayer : players) {
                if (defaultPlayer.getUsername().equals(player)) remainingPlayers3.add(defaultPlayer);
            }
        }
        RoundDto round3 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round3.getRoundType(), equalTo("roundNight"));
        GameDto resultRound3 = null;
        for (int i = 0; i < round3.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(game.getPlayers().get(i));
            voteDto.setVoted(Arrays.asList("testUser4"));
            roundController.vote("1", voteDto);
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound3 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                game = resultRound3;
            }
        }

        game.setCurrentRound("4");
        List<Player> remainingPlayers4 = new ArrayList<>();
        for (String player : resultRound3.getPlayers()) {
            for (Player defaultPlayer : players) {
                if (defaultPlayer.getUsername().equals(player)) remainingPlayers4.add(defaultPlayer);
            }
        }
        RoundDto round4 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round3.getRoundType(), equalTo("roundNight"));
        GameDto resultRound4 = null;
        for (int i = 0; i < round4.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(game.getPlayers().get(i));
            voteDto.setVoted(Arrays.asList("testUser5"));
            roundController.vote("1", voteDto);
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound4 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                game = resultRound4;
            }
        }
        game.setCurrentRound("5");
        List<Player> remainingPlayers5 = new ArrayList<>();
        for (String player : resultRound4.getPlayers()) {
            for (Player defaultPlayer : players) {
                if (defaultPlayer.getUsername().equals(player)) remainingPlayers5.add(defaultPlayer);
            }
        }
        RoundDto round5 = (RoundDto) roundController.getCurrentRound("1").getBody();
        Assert.assertThat(round3.getRoundType(), equalTo("roundNight"));
        GameDto resultRound5 = null;
        for (int i = 0; i < round5.getAvailablePlayers().size(); i++) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(game.getPlayers().get(i));
            voteDto.setVoted(Arrays.asList("testUser6"));
            roundController.vote("1", voteDto);
            ResponseEntity result = roundController.ready("1", game.getPlayers().get(i));
            Assert.assertThat(result.getStatusCode(), equalTo(HttpStatus.OK));
            if (result.getBody().getClass().equals(GameDto.class)) {
                resultRound5 = (GameDto) result.getBody();
                Assert.assertThat(Objects.requireNonNull(result.getBody()).getClass(), equalTo(GameDto.class));
                game = resultRound5;
            }
        }
        Assert.assertNotEquals(null, resultRound5.getWinner());
        Assert.assertThat(resultRound5.getWinner(), equalTo("maffia"));
    }
}
