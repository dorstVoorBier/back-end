package be.kdg.maffia.game.model.dto;

import java.util.List;

public class RoundNightDto extends RoundDto{
    private List<VoteDto> maffiaVotes;
    private List<VoteDto> specialVotes;

    public RoundNightDto(){}

    public List<VoteDto> getMaffiaVotes() {
        return maffiaVotes;
    }

    public void setMaffiaVotes(List<VoteDto> maffiaVotes) {
        this.maffiaVotes = maffiaVotes;
    }

    public List<VoteDto> getSpecialVotes() {
        return specialVotes;
    }
    public void setSpecialVotes(List<VoteDto> specialVotes) {
        this.specialVotes = specialVotes;
    }
}
