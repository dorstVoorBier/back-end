package be.kdg.maffia.game.model.playerroles;

import be.kdg.maffia.game.model.dto.playerroles.*;

public class Citizen extends PlayerRole {

    public Citizen(){super("citizen",false,null);}
    public Citizen(CitizenDto dto){
        super(dto);
    }
}