package be.kdg.maffia.game.model.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;
import java.util.Map;

@JsonRootName("game")
public class GameDto {

    private String gameId;
    private List<String> players;
    private Map<String,String> deadPlayers;
    private int roundCount;
    private String currentRound;
    private List<String> rounds;
    private boolean isDay;
    private String winner;

    public GameDto(){}

    public GameDto(List<String> usernames) {
        this.players = usernames;
        roundCount = 0;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> playernames) {
        this.players = playernames;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public String getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(String currentRound) {
        this.currentRound = currentRound;
    }

    public List<String> getRounds() {
        return rounds;
    }

    public void setRounds(List<String> roundIds) {
        this.rounds = roundIds;
    }


    public Map<String,String> getDeadPlayers() {
        return deadPlayers;
    }

    public void setDeadPlayers(Map<String,String> deadPlayernames) {
        this.deadPlayers = deadPlayernames;
    }

    public void addDeadPlayer(String playername, String rolename) {
        deadPlayers.put(playername,rolename);
    }

    public boolean isDay() {
        return isDay;
    }

    public void setDay(boolean day) {
        isDay = day;
    }

    public void removeAlivePlayer(String playername) {
        players.remove(playername);
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
