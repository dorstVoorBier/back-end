package be.kdg.maffia.game.model;

import be.kdg.maffia.game.model.dto.RoundDto;
import be.kdg.maffia.game.model.dto.RoundNightDto;
import be.kdg.maffia.game.model.dto.VoteDto;
import lombok.NoArgsConstructor;
import be.kdg.maffia.game.model.playerroles.Citizen;
import be.kdg.maffia.game.model.playerroles.Maffia;
import be.kdg.maffia.game.model.votes.Vote;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class RoundNight extends Round{
    private List<Vote> maffiaVotes;
    private List<Vote> powerVotes;


    public RoundNight(String roundId,List<Player> players) {
        super(roundId,players);
        this.maffiaVotes =new ArrayList<>();
        this.powerVotes = new ArrayList<>();
    }

    @Override
    public void addVote(Vote vote) {
        if(vote.getVoter().getRole().getClass().equals(Maffia.class)) this.maffiaVotes.add(vote);
        else if(!vote.getVoter().getRole().getClass().equals(Citizen.class)) this.powerVotes.add(vote);
    }

    public void addPowerVote(Vote vote){
        this.powerVotes.add(vote);
    }

    @Override
    public List<Vote> getVotes() {
        return this.maffiaVotes;
    }

    public List<Vote> getPowerVotes() {
        return this.powerVotes;
    }

    public void setMaffiaVotes(List<Vote> votes){
        maffiaVotes = votes;
    }


    @Override
    public RoundDto toDto() {
        RoundNightDto dto = new RoundNightDto();
        dto.setRoundId(super.getRoundId());
        List<String> usernames = new ArrayList<>();
        for(Player p:super.getAvailablePlayers()){
            usernames.add(p.getUsername());
        }
        dto.setAvailablePlayers(usernames);
        List<VoteDto> maffiaVoteDtos = new ArrayList<>();
        for(Vote vote : getVotes()){
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(vote.getVoter().getUsername());
            voteDto.setVoted(vote.getVoted().stream().map(Player::getUsername).collect(Collectors.toList()));
            maffiaVoteDtos.add(voteDto);
        }
        List<VoteDto> powerVoteDtos = new ArrayList<>();
        for(Vote powerVote:powerVotes){
            powerVoteDtos.add(powerVote.toDto());
        }
        dto.setRoundType("roundNight");
        return dto;
    }
}
