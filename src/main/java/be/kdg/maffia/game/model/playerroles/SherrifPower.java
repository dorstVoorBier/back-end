package be.kdg.maffia.game.model.playerroles;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.Player;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SherrifPower implements RolePower {
    @Override
    public String executePower(Game game, List<Player> p) {
        return p.get(0).getUsername()+ " is a " +p.get(0).getRole().getRolename()+".";
    }
}
