package be.kdg.maffia.game.model;

import be.kdg.maffia.game.model.dto.RoundDayDto;
import be.kdg.maffia.game.model.dto.RoundDto;
import be.kdg.maffia.game.model.dto.VoteDto;
import be.kdg.maffia.game.model.votes.Vote;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class RoundDay extends Round {
    private List<Vote> votes;

    public RoundDay(String roundId, List<Player> players) {
        super(roundId,players);
        votes = new ArrayList<>();
    }

    @Override
    public List<Vote> getVotes() {
        return this.votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    @Override
    public void addVote(Vote vote) {
        votes.add(vote);
    }

    @Override
    public RoundDto toDto() {
        RoundDayDto dto = new RoundDayDto();
        dto.setRoundId(super.getRoundId());
        List<String> usernames = new ArrayList<>();
        for (Player p : super.getAvailablePlayers()) {
            usernames.add(p.getUsername());
        }
        dto.setAvailablePlayers(usernames);
        List<VoteDto> dtos = new ArrayList<>();
        for (Vote vote : votes) {
            VoteDto voteDto = new VoteDto();
            voteDto.setVoter(vote.getVoter().getUsername());
            voteDto.setVoted(vote.getVoted().stream().map(Player::getUsername).collect(Collectors.toList()));
            dtos.add(voteDto);
        }
        dto.setVotes(dtos);
        dto.setRoundType("roundDay");
        return dto;
    }
}
