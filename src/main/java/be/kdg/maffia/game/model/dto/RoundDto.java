package be.kdg.maffia.game.model.dto;


import com.fasterxml.jackson.annotation.*;

import java.util.List;

@JsonTypeInfo(
        use=JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = RoundDayDto.class),
        @JsonSubTypes.Type(value = RoundNightDto.class)
})
@JsonRootName("round")
public abstract class RoundDto {
    private String roundId;
    private List<String> availablePlayers;
    private String roundType;

    private boolean ended;

    public RoundDto() {
    }

    public RoundDto(String roundId, List<String> players) {
        this.roundId = roundId;
        availablePlayers = players;
        ended = false;
    }



    public List<String> getAvailablePlayers() {
        return this.availablePlayers;
    }

    public void setAvailablePlayers(List<String> availablePlayers) {
        this.availablePlayers = availablePlayers;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public String getRoundId() {
        return roundId;
    }

    public void setRoundId(String roundId) {
        this.roundId = roundId;
    }

    public String getRoundType() {
        return roundType;
    }

    public void setRoundType(String roundType) {
        this.roundType = roundType;
    }
}
