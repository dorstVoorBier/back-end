package be.kdg.maffia.game.model.dto;


import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.ArrayList;
import java.util.List;

@JsonRootName("vote")
public class VoteDto {
    private String voter;
    private List<String> voted;

    public VoteDto() {
        voted = new ArrayList<>();
    }
    public String getVoter() {
        return voter;
    }

    public void setVoter(String voter) {
        this.voter = voter;
    }

    public List<String> getVoted() {
        return this.voted;
    }

    public void setVoted(List<String> voted) {
        this.voted = voted;
    }

    public void addVoted(String voted){
        this.voted.add(voted);
    }
}
