package be.kdg.maffia.game.model.dto.playerroles;

import be.kdg.maffia.game.model.dto.playerroles.*;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use=JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = MaffiaDto.class),
        @JsonSubTypes.Type(value = CitizenDto.class)
})
@JsonRootName("playerrole")
public class PlayerRoleDto {
    private String rolename;
    private boolean isEvil;

    public PlayerRoleDto() {
    }

    public PlayerRoleDto(String rolename,boolean isEvil) {
        this.rolename = rolename;
        this.isEvil=isEvil;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public boolean isEvil() {
        return isEvil;
    }

    public void setEvil(boolean evil) {
        isEvil = evil;
    }

}
