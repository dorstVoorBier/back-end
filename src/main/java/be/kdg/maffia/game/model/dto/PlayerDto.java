package be.kdg.maffia.game.model.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("player")
public class PlayerDto {
    private String username;
    private String rolename;

    public PlayerDto(){}

    public PlayerDto(String playerId, String username, String rolename) {
        this.username = username;
        this.rolename = rolename;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
}
