package be.kdg.maffia.game.model.dto.playerroles;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("maffia")
public class MaffiaDto extends PlayerRoleDto {
    public MaffiaDto(){}
    public MaffiaDto(String rolename,boolean isEvil) {
        super(rolename,isEvil);
    }


}
