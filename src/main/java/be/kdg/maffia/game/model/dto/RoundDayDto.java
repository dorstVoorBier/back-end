package be.kdg.maffia.game.model.dto;


import java.util.List;


public class RoundDayDto extends RoundDto {
    private List<VoteDto> votes;

    public RoundDayDto() {

    }

    public List<VoteDto> getVotes() {
        return votes;
    }

    public void setVotes(List<VoteDto> votes) {
        this.votes = votes;
    }

    public void addVote(VoteDto vote) {
        votes.add(vote);
    }

}
