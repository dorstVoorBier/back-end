package be.kdg.maffia.game.model;

import be.kdg.maffia.game.model.dto.PlayerDto;
import be.kdg.maffia.game.model.playerroles.PlayerRole;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("player")
public class Player {
    @Id
    private String playerId;
    private String username;
    private PlayerRole role;
    private PlayerState state;

    public Player() {
        this.state = new PlayerState();
    }

    public Player(String username) {
        this.username = username;
        this.state= new PlayerState();
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PlayerRole getRole() {
        return role;
    }

    public void setRole(PlayerRole role) {
        this.role = role;
    }

    public PlayerState getState() {
        return state;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }

    public PlayerDto toDto(){
        PlayerDto dto = new PlayerDto();
        dto.setUsername(this.username);
        if(this.role!=null){
            dto.setRolename(this.role.getRolename());
        }
        return dto;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(Player.class)){
            Player p = (Player)obj;
            return this.getUsername().equals(p.getUsername()) && this.getRole().getRolename().equals(p.getRole().getRolename());
        }else return false;
    }
}
