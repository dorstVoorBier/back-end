package be.kdg.maffia.game.model;


import be.kdg.maffia.game.model.dto.RoundDto;
import be.kdg.maffia.game.model.votes.Vote;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("round")
public abstract class Round {
    @Id
    private String roundId;
    private int roundOfGame;
    private List<Player> availablePlayers;

    private boolean ended;

    public Round() {
        availablePlayers = new ArrayList<>();
    }

    public Round(String roundId,List<Player> players) {
        this.roundId=roundId;
        availablePlayers = players;
        ended = false;
    }


    public List<Player> getAvailablePlayers() {
        return availablePlayers;
    }

    public void setAvailablePlayers(List<Player> availablePlayers) {
        this.availablePlayers = availablePlayers;
    }


    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public String getRoundId() {
        return roundId;
    }

    public void setRoundId(String roundId) {
        this.roundId = roundId;
    }

    public int getRoundOfGame() {
        return roundOfGame;
    }

    public void setRoundOfGame(int roundOfGame) {
        this.roundOfGame = roundOfGame;
    }

    public abstract void addVote(Vote vote);
    public abstract List<Vote> getVotes();
    public abstract RoundDto toDto();
}