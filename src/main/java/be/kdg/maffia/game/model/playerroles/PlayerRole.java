package be.kdg.maffia.game.model.playerroles;

import be.kdg.maffia.game.model.dto.playerroles.PlayerRoleDto;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("playerrole")
public abstract class PlayerRole {
    @Id
    private String roleId;
    private String rolename;
    private boolean isEvil;
    private RolePower rolePower;

    public PlayerRole() {
    }

    public PlayerRole(PlayerRoleDto dto){
        this.rolename=dto.getRolename();
    }

    public PlayerRole(String rolename,boolean isEvil,RolePower power) {
        this.rolename = rolename;
        this.isEvil=isEvil;
        this.rolePower=power;
    }

    public String getRolename() {
        return rolename;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean isEvil() {
        return isEvil;
    }

    public void setEvil(boolean evil) {
        isEvil = evil;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public RolePower getRolePower() {
        return rolePower;
    }

    public void setRolePower(RolePower rolePower) {
        this.rolePower = rolePower;
    }
}