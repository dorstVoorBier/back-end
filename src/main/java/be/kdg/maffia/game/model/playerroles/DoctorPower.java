package be.kdg.maffia.game.model.playerroles;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.Player;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DoctorPower implements RolePower {
    @Override
    public String executePower(Game game , List<Player> p) {
        Player player = null;
        for (Player gamePlayer : game.getCurrentRound().getAvailablePlayers()){
            if (gamePlayer.equals(p.get(0))){
                gamePlayer.getState().setProtected(true);
                return "You protected "+gamePlayer.getUsername();
            }
        }
        return "";
    }
}
