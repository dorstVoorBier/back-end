package be.kdg.maffia.game.model.dto.playerroles;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("citizen")
public class CitizenDto extends PlayerRoleDto {

    public CitizenDto(){}
    public CitizenDto(String rolename,boolean isEvil) {
        super(rolename,isEvil);
    }
}
