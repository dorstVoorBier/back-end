package be.kdg.maffia.game.model.playerroles;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.Player;

import java.util.List;

public interface RolePower {
    String executePower(Game game, List<Player> p);
}
