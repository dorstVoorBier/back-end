package be.kdg.maffia.game.model.playerroles;

import be.kdg.maffia.game.model.dto.playerroles.*;

public class Maffia extends PlayerRole {
    public Maffia(){
        super("maffia",true,null);
    }
    public Maffia(MaffiaDto dto){
        super(dto);
    }
}