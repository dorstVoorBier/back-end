package be.kdg.maffia.game.model;


import be.kdg.maffia.game.model.dto.GameDto;
import be.kdg.maffia.game.model.playerroles.PlayerRole;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Document("game")
public class Game {

    @Id
    private String gameId;
    private List<Player> citizens;
    private List<Player> maffias;
    private List<Player> deadPlayers;
    private List<PlayerRole> allRoles;
    private int roundCount;
    private List<Round> rounds;
    private Round currentRound;
    private boolean isDay;
    private PlayerRole winner;

    public Game(){}

    public Game(List<Player> players){
        this.citizens = new ArrayList<>();
        this.maffias = new ArrayList<>();
        this.deadPlayers=new ArrayList<>();
        this.roundCount=0;
        this.rounds = new ArrayList<>();
        this.allRoles = new ArrayList<>();
        isDay=true;
        for(Player p:players){
            if(p.getRole().isEvil())maffias.add(p);
            else citizens.add(p);
        }
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<Player> getCitizens() {
        return citizens;
    }

    public void setCitizens(List<Player> citizens) {
        this.citizens = citizens;
    }

    public List<Player> getMaffias() {
        return maffias;
    }

    public void setMaffias(List<Player> maffias) {
        this.maffias = maffias;
    }

    public List<Player> getAvailablePlayers(){
        return Stream.concat(citizens.stream(),maffias.stream()).collect(Collectors.toList());
    }

    public Round getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(Round currentRound) {
        if(this.currentRound!=null){
            this.currentRound.setEnded(true);
            addRound(currentRound);
        }
        currentRound.setRoundOfGame(roundCount);
        this.currentRound = currentRound;
    }

    public int getRoundCount() {
        return roundCount;
    }

    public void setRoundCount(int roundCount) {
        this.roundCount = roundCount;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public Round getRound(String roundId){
        for(Round round : rounds){
            if(round.getRoundId().equals(roundId))return round;
        }
        return null;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public void addRound(Round round) {
        if(null==rounds){
            rounds=new ArrayList<>();
        }
        rounds.add(round);
    }

    public void nextRound() {
        this.isDay=!this.isDay;
        this.roundCount++;
    }

    public List<Player> getDeadPlayers() {
        return deadPlayers;
    }

    public void setDeadPlayers(List<Player> deadPlayers) {
        this.deadPlayers = deadPlayers;
    }

    public void addDeadPlayer(Player player) {
        deadPlayers.add(player);
    }

    public boolean isDay() {
        return isDay;
    }

    public void setDay(boolean day) {
        isDay = day;
    }

    public void removeAlivePlayer(Player player) {
        citizens.remove(player);
        maffias.remove(player);
    }

    public List<PlayerRole> getAllRoles() {
        return allRoles;
    }

    public PlayerRole getWinner() {
        return winner;
    }
    public void setWinner(PlayerRole winnerRole){
        this.winner = winnerRole;
    }

    public void setAllRoles(List<PlayerRole> allRoles) {
        this.allRoles = allRoles;
    }

    public GameDto toDto(){
        GameDto dto = new GameDto();
        dto.setGameId(this.gameId);
        if(null != currentRound){
            dto.setCurrentRound(currentRound.getRoundId());
        }
        dto.setRoundCount(this.getRoundCount());
        List<String> usernames = new ArrayList<>();
        for(Player p: citizens){
            usernames.add(p.getUsername());
        }
        for(Player p:maffias){
            usernames.add(p.getUsername());
        }
        dto.setPlayers(usernames);
        Map<String,String> deadPlayers  = new HashMap<>();
        for(Player p2:this.deadPlayers){
            deadPlayers.put(p2.getUsername(),p2.getRole().getRolename());
        }
        dto.setDeadPlayers(deadPlayers);
        List<String> rounds = new ArrayList<>();
        for(Round r:this.rounds){
            rounds.add(r.getRoundId());
        }
        dto.setRounds(rounds);
        dto.setDay(isDay);
        if(winner!=null) dto.setWinner(winner.getRolename());
        return dto;
    }
}
