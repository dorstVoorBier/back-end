package be.kdg.maffia.game.model.votes;

import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.game.model.dto.VoteDto;

import java.util.List;
import java.util.Objects;

public class Vote {
    private Player voter;
    private List<Player> voted;

    public Vote(Player voter, List<Player> voted) {
        this.voter=voter;
        this.voted = voted;
    }

    public Player getVoter() {
        return voter;
    }

    public void setVoter(Player voter) {
        this.voter = voter;
    }

    public List<Player> getVoted(){
        return this.voted;
    }

    public VoteDto toDto() {
        VoteDto dto = new VoteDto();
        dto.setVoter(this.getVoter().getUsername());
        this.getVoted().forEach(p -> dto.addVoted(p.getUsername()));
        return dto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return Objects.equals(voter.getUsername(), vote.voter.getUsername());
    }
}
