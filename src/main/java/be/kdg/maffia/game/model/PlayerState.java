package be.kdg.maffia.game.model;

/**
 * Subclass of Player which persists the state of the Player. The content of this Class may expand drastically based on
 * the addition of new Roles.
 */
public class PlayerState {
    /*  true if the player has been chosen to be protected by the doctor */
    private boolean isProtected;
    /*  Player that has been linked by Cupid */
    private Player linkedPlayer;
    /*  Specific roles might not be killable during the night  */
    private boolean isKillableAtNight;
    /*  true if the player locked his vote, false if player still has to lock in his vote */
    private boolean isReady;

    public PlayerState() {
        this.isProtected=false;
        this.isKillableAtNight=true;
        this.isReady=false;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public void setProtected(boolean aProtected) {
        isProtected = aProtected;
    }

    public Player getLinkedPlayer() {
        return linkedPlayer;
    }

    public void setLinkedPlayer(Player linkedPlayer) {
        this.linkedPlayer = linkedPlayer;
    }

    public boolean isKillableAtNight() {
        return isKillableAtNight;
    }

    public void setKillableAtNight(boolean killableAtNight) {
        isKillableAtNight = killableAtNight;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }
}
