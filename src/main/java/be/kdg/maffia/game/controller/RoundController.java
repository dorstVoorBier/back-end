package be.kdg.maffia.game.controller;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.RoundNight;
import be.kdg.maffia.game.model.dto.VoteDto;
import be.kdg.maffia.game.model.votes.Vote;
import be.kdg.maffia.game.services.interfaces.GameService;
import be.kdg.maffia.game.services.interfaces.RoundService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Handles Requests specifically for Rounds of a Game. Most HTTP-traffic flows through here.
 */
@Controller
@CrossOrigin
public class RoundController {

    private GameService gameService;
    private RoundService roundService;
    private SimpMessagingTemplate simpMessagingTemplate;

    public RoundController(GameService gameService, RoundService roundService, SimpMessagingTemplate simpMessagingTemplate) {
        this.gameService = gameService;
        this.roundService = roundService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/game/{gameId}/getcurrentround")
    public ResponseEntity getCurrentRound(@DestinationVariable String gameId) {
        try {
            return ResponseEntity.ok(gameService.getGame(gameId).getCurrentRound().toDto());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/round", method = RequestMethod.GET)
    public ResponseEntity getRound(@RequestParam(name="gameId") String gameId, @RequestParam(name = "roundCount") int roundId) {
        try {
            return ResponseEntity.ok(roundService.getRoundById(gameId,roundId).toDto());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/getvotes/{gameid}/{role}", method = RequestMethod.POST)
    public ResponseEntity getVotes(@PathVariable(name = "gameid") String gameId, @PathVariable(name = "role") String roleName) {
        try {
            return ResponseEntity.ok(roundService.getVotes(gameId,roleName).stream().map(vote -> vote.toDto()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


    @MessageMapping("game/{gameid}/vote")
    public ResponseEntity vote(@DestinationVariable String gameid, VoteDto voteInput) {
        try {
            Game game = gameService.getGame(gameid);
            Vote vote = roundService.addVote(game,voteInput.getVoter(), voteInput.getVoted());
            String roleName =gameService.getPlayerByUserName(gameid,voteInput.getVoter()).getRole().getRolename();
            if (game.isDay()) {
                simpMessagingTemplate.convertAndSend("/topic/" + gameid + "/citizen", ResponseEntity.ok(vote.toDto()));
            }else {
                simpMessagingTemplate.convertAndSend("/topic/" + gameid + "/" + roleName, ResponseEntity.ok(vote.toDto()));
            }
            return ResponseEntity.ok(vote.toDto());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getCause());
        }
    }

    /*
    Setting a player's state to ready to allow the round to end.
     */
    @MessageMapping("/game/{gameid}/voteready/{username}")
    @SendTo("/topic/game/{gameid}/voteready")
    public ResponseEntity ready(@DestinationVariable String gameid, @DestinationVariable String username) {
        try {
            Game game = gameService.getGame(gameid);
            int readyCount = roundService.addReady(game, username);
            if (readyCount == 0) {
                return this.endRound(game);
            } else {
                return ResponseEntity.ok(readyCount);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    /*
    Ends the round based on its state (Day or Night). When the Round is Night, appropriate roles get their selected result. After that, the maffiaVotes are handled and a new round will start.
     */
    private ResponseEntity endRound(Game game) {
        try {
            if (!game.isDay() && game.getCurrentRound().getClass().equals(RoundNight.class) ) {
                sendPowerResults(game);
            }
            Game roundResult = roundService.endRound(game);
            simpMessagingTemplate.convertAndSend("/topic/game", ResponseEntity.ok(roundResult.toDto()));
            return ResponseEntity.ok(roundResult.toDto());
        } catch (Exception e) {
            simpMessagingTemplate.convertAndSend("/topic/game", ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage()));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    /**
     * Sends information to related WebSocket. This will be done based on the Role of the Player.
     */
    private void sendPowerResults(Game game) {
        String role="";
        try {
            RoundNight currentRound = (RoundNight)game.getCurrentRound();
            List<String> powerResults = new ArrayList<>();
            for (Vote vote : currentRound.getPowerVotes()) {
                role = vote.getVoter().getRole().getRolename();
                String result = roundService.handlePowerVote(game, vote);
                simpMessagingTemplate.convertAndSend("/topic/game/"+role,ResponseEntity.ok(result));
            }
        } catch (Exception e) {
            if(!"".equals(role)){
                simpMessagingTemplate.convertAndSend("/topic/game/"+role, ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage()));
            }
        }
    }
}
