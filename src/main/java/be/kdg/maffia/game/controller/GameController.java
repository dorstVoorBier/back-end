package be.kdg.maffia.game.controller;


import be.kdg.maffia.game.model.dto.GameDto;
import be.kdg.maffia.game.model.dto.PlayerDto;
import be.kdg.maffia.game.services.interfaces.GameService;
import be.kdg.maffia.game.services.interfaces.PlayerService;
import be.kdg.maffia.game.services.interfaces.RoundService;
import be.kdg.maffia.room.services.RoomService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin
public class GameController {

    private GameService gameService;

    private PlayerService playerService;
    private RoomService roomService;
    private SimpMessagingTemplate simpMessagingTemplate;

    public GameController(GameService gameService, RoundService roundService, PlayerService playerService, RoomService roomService, SimpMessagingTemplate simpMessagingTemplate) {
        this.gameService = gameService;
        this.playerService = playerService;
        this.roomService = roomService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/startgame/{roomid}")
    @SendTo("/topic/game/{roomid}")
    public ResponseEntity startGame(@DestinationVariable String roomid) {
        try {
            List<String> playernames = roomService.getRoom(roomid).getPlayers();
            System.out.println(playernames);
            GameDto gameDto = gameService.startNewGame(playernames).toDto();
            return ResponseEntity.ok(gameDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/player/{gameid}/{username}", method = RequestMethod.POST)
    public ResponseEntity getPlayer(@PathVariable( name = "gameid" ) String gameId, @PathVariable(name = "username") String username) {
        try {
            PlayerDto playerOut = gameService.getPlayerByUserName(gameId,username).toDto();
            return ResponseEntity.ok(playerOut);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
