package be.kdg.maffia.game.exceptions;

public class PlayerRoleNotFoundException extends RuntimeException {
    public PlayerRoleNotFoundException() {
    }

    public PlayerRoleNotFoundException(String message) {
        super(message);
    }

    public PlayerRoleNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
