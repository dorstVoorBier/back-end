package be.kdg.maffia.game.exceptions;

public class RoundServiceException extends RuntimeException {
    public RoundServiceException() {
    }

    public RoundServiceException(String message) {
        super(message);
    }

    public RoundServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
