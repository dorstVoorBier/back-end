package be.kdg.maffia.game.exceptions;

public class PlayerRoleServiceException extends RuntimeException {
    public PlayerRoleServiceException() {
    }

    public PlayerRoleServiceException(String message) {
        super(message);
    }

    public PlayerRoleServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
