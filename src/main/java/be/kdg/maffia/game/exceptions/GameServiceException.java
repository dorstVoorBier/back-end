package be.kdg.maffia.game.exceptions;

public class GameServiceException extends RuntimeException {
    public GameServiceException() {
    }

    public GameServiceException(String message) {
        super(message);
    }

    public GameServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
