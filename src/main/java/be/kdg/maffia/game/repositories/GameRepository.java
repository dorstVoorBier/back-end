package be.kdg.maffia.game.repositories;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.Player;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Game instances.
 */
@Repository
public interface GameRepository extends MongoRepository<Game,String> {
    Game findByGameId(String gameId);
}
