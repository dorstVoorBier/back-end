package be.kdg.maffia.game.repositories;

import be.kdg.maffia.game.model.playerroles.PlayerRole;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for persisting and finding PlayerRoles.
 */
@Repository
public interface PlayerRoleRepository extends MongoRepository<PlayerRole,String> {
    PlayerRole getByRolename(String rolename);
    List<PlayerRole> findAll();
}
