package be.kdg.maffia.game.repositories;

import be.kdg.maffia.game.model.Player;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for Player instances.
 */
@Repository
public interface PlayerRepository extends MongoRepository<Player,String> {
    Player findByPlayerId(String id);
    Player findByUsername(String username);
}
