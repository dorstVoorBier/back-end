package be.kdg.maffia.game.services;

import be.kdg.maffia.game.exceptions.PlayerServiceException;
import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.game.repositories.PlayerRepository;
import be.kdg.maffia.game.services.interfaces.PlayerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {

    private PlayerRepository playerRepository;
    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public Player getPlayerByUsername(String username) {
        try {
            return playerRepository.findByUsername(username);
        } catch (Exception e) {
            throw new PlayerServiceException("Error while fetching user with username: " + username, e.getCause());
        }
    }
    public Player getPlayerById(String id){
        try{
            return playerRepository.findByPlayerId(id);
        }catch (Exception e){
            throw new PlayerServiceException("Error fetching user with id: "+id,e.getCause());
        }
    }

    @Override
    public Player addNewPlayer(String username) {
        try {
            Player p = new Player();
            p.setUsername(username);
            playerRepository.save(p);
            return p;
        }catch (Exception e){
            throw new PlayerServiceException("Could not add new player",e.getCause());
        }
    }

    @Override
    public void refreshPlayerStates(List<Player> players) {
        for(Player p:players){
            p.getState().setReady(false);
            p.getState().setProtected(false);
        }
    }
}
