package be.kdg.maffia.game.services;

import be.kdg.maffia.game.exceptions.GameServiceException;
import be.kdg.maffia.game.model.*;
import be.kdg.maffia.game.repositories.GameRepository;
import be.kdg.maffia.game.services.interfaces.GameService;
import be.kdg.maffia.game.services.interfaces.PlayerRoleService;
import be.kdg.maffia.game.services.interfaces.PlayerService;
import be.kdg.maffia.game.services.interfaces.RoundService;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class GameServiceImpl implements GameService {
    private PlayerRoleService playerRoleService;
    private PlayerService playerService;
    private RoundService roundService;
    private GameRepository gameRepository;
    private ProfileService profileService;

    public GameServiceImpl(PlayerRoleService playerRoleService, PlayerService playerService, RoundService roundService, GameRepository gameRepository, ProfileService profileService) {
        this.playerRoleService = playerRoleService;
        this.playerService = playerService;
        this.roundService = roundService;
        this.gameRepository = gameRepository;
        this.profileService = profileService;
    }

    /**
     * Create games based on given Usernames. Creates the first Round and persists data.
     * @param usernames Used to create Player-objects.
     * @return Game with new players, new Round and roles are set
     */
    @Override
    public Game startNewGame(List<String> usernames) {
        List<Player> players = new ArrayList<>();
        try {
            for (String username : usernames) {
                players.add(playerService.addNewPlayer(username));
            }
            profileService.updateGamesPlayed(players);
            Game newGame = new Game(playerRoleService.setRoles(players));
            newGame.setAllRoles(playerRoleService.getAllRoles());
            roundService.startNewRound(newGame);
            gameRepository.save(newGame);
            return newGame;

        } catch (Exception e) {
            throw new GameServiceException("Unexpected Error while starting new game", e.getCause());
        }
    }


    @Override
    public Game getGame(String gameId) {
        return gameRepository.findByGameId(gameId);
    }

    @Override
    public Player getPlayerByUserName(String gameId, String username) {
        try {
            for (Player player : this.getGame(gameId).getAvailablePlayers()
                    ) {
                if (player.getUsername().equals(username)) {
                    return player;
                }
            }
            //todo fix bad thown exception in case player not alive in game
            throw new Exception("player not found in game");
        } catch (Exception e) {
            throw new GameServiceException("Could not fetch player: " + username, e.getCause());
        }
    }
}
