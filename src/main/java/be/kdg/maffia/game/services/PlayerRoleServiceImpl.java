package be.kdg.maffia.game.services;

import be.kdg.maffia.game.exceptions.PlayerRoleServiceException;
import be.kdg.maffia.game.exceptions.PlayerRoleNotFoundException;
import be.kdg.maffia.game.model.*;
import be.kdg.maffia.game.model.playerroles.*;
import be.kdg.maffia.game.repositories.PlayerRepository;
import be.kdg.maffia.game.repositories.PlayerRoleRepository;
import be.kdg.maffia.game.services.interfaces.PlayerRoleService;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class PlayerRoleServiceImpl implements PlayerRoleService {

    private PlayerRoleRepository roleRepository;
    private PlayerRepository playerRepository;
    private ProfileService profileService;

    public PlayerRoleServiceImpl(PlayerRoleRepository roleRepository, PlayerRepository playerRepository, ProfileService profileService) {
        this.roleRepository = roleRepository;
        this.playerRepository = playerRepository;
        this.profileService = profileService;
    }

    @Override
    public PlayerRole getRoleByName(String rolename) {
        try {
            PlayerRole role =  roleRepository.getByRolename(rolename.toLowerCase());
            if (role==null) throw new PlayerRoleNotFoundException("No PlayerRole found for: "+rolename);
            return role;
        } catch (Exception e) {
            throw new PlayerRoleServiceException("Error while fetching Role by rolename: " + rolename, e.getCause());
        }

    }

    public List<PlayerRole> getAllRoles() {
        try {
            return roleRepository.findAll();
        } catch (Exception e) {
            throw new PlayerRoleServiceException("Error while fetching all roles", e.getCause());
        }
    }

    /**
     * Distributes roles to every Player in the given List. The first X players of the list will be Maffia.
     * X is based on the amount of players:
     * 7 - 10  Players:   2 Maffia's
     * 11 - 13 Players:   3 Maffia's
     * 14 - 17 Players:   4 Maffia's
     * 18 - 20 Players:   5 Maffia's
     * @param players a minimum of 7 Players is required to successfully distribute roles. Maximum of 20.
     * @return List<Players> with a valid role set. Shuffled.
     */
    @Override
    public List<Player> setRoles(List<Player> players) {
        try {
            PlayerRole roleMaffia = getRoleByName("maffia");
            PlayerRole roleCitizen = getRoleByName("citizen");
            PlayerRole roleDoctor = getRoleByName("doctor");
            PlayerRole roleSherrif = getRoleByName("sherrif");
            if (roleMaffia == null) {
                roleMaffia = new Maffia();
                roleRepository.save(roleMaffia);
            }
            if (roleCitizen == null) {
                roleCitizen = new Citizen();
                roleRepository.save(roleCitizen);
            }
            if (roleDoctor == null) {
                roleDoctor = new Doctor();
                roleRepository.save(roleDoctor);
            }
            if (roleSherrif == null) {
                roleSherrif = new Sherrif();
                roleRepository.save(roleSherrif);
            }

            Collections.shuffle(players);
            int maffiaCount = 0;

            if (players.size() >= 7 && players.size() <= 10) maffiaCount = 2;
            if (players.size() >= 11 && players.size() <= 13) maffiaCount = 3;
            if (players.size() >= 14 && players.size() <= 18) maffiaCount = 4;
            if (players.size() >= 18 && players.size() <= 20) maffiaCount = 5;

            for (int i = 0; i < maffiaCount; i++) {
                players.get(i).setRole(roleMaffia);
                profileService.updateMaffiaGames(players.get(i).getUsername());
                playerRepository.save(players.get(i));
            }
            //Setting Doctor Role
            players.get(maffiaCount).setRole(roleDoctor);
            profileService.updateDoctorGames(players.get(maffiaCount).getUsername());
            //Setting Sherrif Role
            players.get(maffiaCount+1).setRole(roleSherrif);
            profileService.updateSherrifGames(players.get(maffiaCount+1).getUsername());
            for (int i = maffiaCount+2; i < players.size(); i++) {
                players.get(i).setRole(roleCitizen);
                profileService.updateCitizenGames(players.get(i).getUsername());
                playerRepository.save(players.get(i));
            }

            Collections.shuffle(players);
            return players;
        } catch (Exception e) {
            throw new PlayerRoleServiceException("Error while setting roles for list of players", e.getCause());
        }

    }
}
