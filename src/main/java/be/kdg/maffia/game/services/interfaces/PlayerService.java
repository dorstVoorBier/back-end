package be.kdg.maffia.game.services.interfaces;

import be.kdg.maffia.game.model.Player;

import java.util.List;

public interface PlayerService {
    Player getPlayerByUsername(String username);
    Player getPlayerById(String id);
    Player addNewPlayer(String username);
    void refreshPlayerStates(List<Player> players);
}
