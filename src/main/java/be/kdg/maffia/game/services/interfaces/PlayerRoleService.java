package be.kdg.maffia.game.services.interfaces;

import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.game.model.playerroles.PlayerRole;

import java.util.List;

public interface PlayerRoleService {
    PlayerRole getRoleByName(String rolename);
    List<Player> setRoles(List<Player> players);
    List<PlayerRole> getAllRoles();
}
