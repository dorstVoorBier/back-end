package be.kdg.maffia.game.services.interfaces;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.game.model.Round;


import java.util.List;

/**
 * Handles requests for Game Entities.
 */
public interface GameService {
    Game startNewGame(List<String> usernames);
    Game getGame(String gameId);
    Player getPlayerByUserName(String gameId,String username);
}
