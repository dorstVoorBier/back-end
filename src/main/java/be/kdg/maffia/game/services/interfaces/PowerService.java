package be.kdg.maffia.game.services.interfaces;

import be.kdg.maffia.game.model.Player;

public interface PowerService {
    Player executePower(Player p);
}
