package be.kdg.maffia.game.services.interfaces;

import be.kdg.maffia.game.model.Game;
import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.game.model.Round;
import be.kdg.maffia.game.model.dto.VoteDto;
import be.kdg.maffia.game.model.votes.Vote;

import java.util.List;

/**
 * Handles the core logic of the game. Adding Votes, killing players, deciding the winners, etc...
 */
public interface RoundService {
    Vote addVote(Game game, String usernameVoter, List<String> usernameVoted);
    Round getRoundById(String gameId,int roundCout);
    Player getMostVoted(Round round);
    int addReady(Game game, String userName);
    String handlePowerVote(Game game, Vote vote);
    void startNewRound(Game game);
    Game endRound(Game game);
    List<Vote> getVotes(String roundId, String roleName);
}
