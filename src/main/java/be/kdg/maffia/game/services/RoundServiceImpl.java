package be.kdg.maffia.game.services;

import be.kdg.maffia.game.exceptions.GameServiceException;
import be.kdg.maffia.game.exceptions.RoundServiceException;
import be.kdg.maffia.game.model.*;
import be.kdg.maffia.game.model.playerroles.Citizen;
import be.kdg.maffia.game.model.playerroles.Maffia;
import be.kdg.maffia.game.model.playerroles.PlayerRole;
import be.kdg.maffia.game.model.votes.Vote;
import be.kdg.maffia.game.repositories.GameRepository;
import be.kdg.maffia.game.services.interfaces.PlayerRoleService;
import be.kdg.maffia.game.services.interfaces.PlayerService;
import be.kdg.maffia.game.services.interfaces.RoundService;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RoundServiceImpl implements RoundService {
    private GameRepository gameRepository;
    private PlayerService playerService;
    private PlayerRoleService roleService;
    private ProfileService profileService;

    public RoundServiceImpl(GameRepository gameRepository, PlayerService playerService, PlayerRoleService roleService, ProfileService profileService) {
        this.gameRepository = gameRepository;
        this.playerService = playerService;
        this.roleService = roleService;
        this.profileService = profileService;
    }


    /**
     * Add vote based on the state of the Game (Day or Night). When Night the vote will be added to the correct collection (Maffia votes or Powervotes)
     * based on the Role of the Voter.
     *
     * @param game           game to add vote to
     * @param usernameVoter  Username of player who picked the vote
     * @param usernamesVoted List of usernames that got voted
     * @return Created vote with its respectible Players.
     * @throws GameServiceException when Add failes, mostly caused by Players that could not be found.
     */
    @Override
    public Vote addVote(Game game, String usernameVoter, List<String> usernamesVoted) throws GameServiceException {

        Player voter = null;
        List<Player> voted = new ArrayList<>();

        for (Player p : game.getCurrentRound().getAvailablePlayers()) {
            if (p.getUsername().equals(usernameVoter)) {
                voter = p;
            }
            for (String userNameVoted : usernamesVoted) {
                if (p.getUsername().equals(userNameVoted)) {
                    voted.add(p);
                    break;
                }
            }
        }
        if (voter != null && voted != null) {
            Vote vote = new Vote(voter, voted);
            game.getCurrentRound().addVote(vote);
            gameRepository.save(game);
            return vote;
        } else {
            throw new GameServiceException("Could not create vote for given players");
        }
    }

    public Round getRoundById(String gameId, int roundCount) {
        try {
            Game game = gameRepository.findByGameId(gameId);
            return game.getRounds().get(roundCount);
        } catch (Exception e) {
            throw new RoundServiceException("Could not fetch round with id: " + roundCount + " in game: " + gameId, e.getCause());
        }
    }

    /**
     * Returns Player that got the most votes during given Round. Based on received Votes of 'addVote'.
     *
     * @param round Round to get Player from
     * @return Player with most Votes.
     */
    @Override
    public Player getMostVoted(Round round) {
        List<Vote> votes = new ArrayList<>();
        for (Vote vote : round.getVotes()) {
            if (!votes.contains(vote)) {
                votes.add(vote);
            } else {
                votes.remove(vote);
                votes.add(vote);
            }
        }

        Map<Player, Integer> voteCount = new HashMap<>();
        for (Player p : round.getAvailablePlayers()) {
            voteCount.put(p, 0);
            for (Vote vote : votes) {
                if(vote.getVoter()!=null && vote.getVoted()!=null){
                    for (Player votedPlayer : vote.getVoted()) {
                        if (votedPlayer.equals(p)) voteCount.put(p, voteCount.get(p) + 1);
                    }
                }
            }
        }

        Player topVoted = null;
        for (Player p : voteCount.keySet()) {
            if (topVoted != null) {
                if (voteCount.get(p) > voteCount.get(topVoted)) topVoted = p;
            } else {
                if (voteCount.get(p) != 0) topVoted = p;
            }
        }
        return topVoted;
    }

    /**
     * Returns a list of all the most recent votes of each unique player still alive.
     *
     * @param gameId Needed to get the votes from the right game
     * @param roleName A rolename is needed so a certain role can't see another role's votes
     * @return a List of Votes
     */
    @Override
    public List<Vote> getVotes(String gameId, String roleName) {
        Round round = gameRepository.findByGameId(gameId).getCurrentRound();
        List<Vote> votes = new ArrayList<>();
        // if it is night and you are NOT a citizen or a maffia, you access the powervote list
        if (round.getClass() == RoundNight.class && !(roleName.equals("citizen") || roleName.equals("maffia"))) {
            RoundNight roundNight = (RoundNight) round;
            for (Vote vote : roundNight.getPowerVotes()) {
                //if the voters role equals your own role you get to see the vote
                if (vote.getVoter().getRole().getRolename().equals(roleName)) {
                    if (!votes.contains(vote)) {
                        votes.add(vote);
                    } else {
                        votes.remove(vote);
                        votes.add(vote);
                    }
                }
            }
        } else {
            for (Vote vote : round.getVotes()) {
                if (!votes.contains(vote)) {
                    votes.add(vote);
                } else {
                    votes.remove(vote);
                    votes.add(vote);
                }
            }
        }
        return votes;
    }


    /**
     * Sets a Player in given game to Ready. This means they locked in their vote and can't change it.
     *
     * @param game     Game to set Player to ready
     * @param userName username of player to set ready.
     * @return Amount of players ready.
     */
    public int addReady(Game game, String userName) {
        for (Player p : game.getCurrentRound().getAvailablePlayers()) {
            if (p.getUsername().equals(userName)) {
                p.getState().setReady(true);
                break;
            }
        }
        gameRepository.save(game);
        return game.getCurrentRound().getAvailablePlayers().size() - getAmountReady(game.getCurrentRound());
    }

    /**
     * Executes the RolePower of every PowerVote in given Round.
     *
     * @param vote PowerVote during RoundNight.
     * @return Message of PowerVote;
     */
    public String handlePowerVote(Game game, Vote vote) {
        PlayerRole role = vote.getVoter().getRole();
        String result = role.getRolePower().executePower(game, vote.getVoted());
        gameRepository.save(game);
        return result;
    }

    /**
     * Starts a new Round based on the state of isDay of given Game
     *
     * @param game Game to start a new round with.
     * @return new Round, RoundDay if Game.isDay = true. RoundNight is Game.isDay = false;
     */
    @Override
    public void startNewRound(Game game) {
        game.nextRound();
        Round round;
        if (game.isDay()) {
            round = new RoundDay(Integer.toString(game.getRoundCount()), game.getAvailablePlayers());
        } else {
            round = new RoundNight(Integer.toString(game.getRoundCount()), game.getAvailablePlayers());
        }
        game.setCurrentRound(round);
        if(!game.isDay()){
            List<String> citizens = new ArrayList<>();
            for (Player player: game.getAvailablePlayers()) {
                if("citizen".equals(player.getRole().getRolename())){
                    citizens.add(player.getUsername());
                }
            }
            for (String username: citizens) {
                addReady(game, username);
            }
        }
    }

    /**
     * Ends current round of given Game. Decides the PlayerToKill based on the Votes of current Round. This player will be killed unless the player is protected by a Doctor-Role.
     * When the player is killed, the winner is selected based on SelectWinner.
     *
     * @param game Game to end Round of.
     * @return Game with a potential Dead Player and/or a winning role.
     */
    @Override
    public Game endRound(Game game) {
        try {
            Player playerToKill = getMostVoted(game.getCurrentRound());
            if (playerToKill != null) {
                if (!playerToKill.getState().isProtected()) {
                    killPlayer(playerToKill, game);
                }
            }
            PlayerRole winner = selectWinner(game);
            if (winner != null) {
                if (winner.getClass().equals(Maffia.class)){
                    profileService.updateMaffiaWins(game.getMaffias());
                }else if (winner.getClass().equals(Citizen.class)){
                    profileService.updateCitizenWins(game.getCitizens());
                }
                game.setWinner(winner);
            } else {
                playerService.refreshPlayerStates(game.getAvailablePlayers());
                startNewRound(game);
            }
            gameRepository.save(game);
            return game;
        } catch (Exception e) {
            throw new GameServiceException("Error end voting round", e.getCause());
        }
    }

    /**
     * Adds the given Player to Game.DeadPlayers, and removing him form Game.Players
     *
     * @param player player to kill
     * @param game   game with dead player.
     */
    private void killPlayer(Player player, Game game) {
        try {
            if (game.getDeadPlayers() == null) {
                game.setDeadPlayers(new ArrayList<>());
            }
            game.addDeadPlayer(player);
            game.removeAlivePlayer(player);
            game.getCurrentRound().setAvailablePlayers(game.getAvailablePlayers());
        } catch (Exception e) {
            throw new GameServiceException("Error handling killPlayer command for player: " + player.getUsername(), e.getCause());
        }
    }

    private PlayerRole selectWinner(Game game) {
        if (game.getCitizens().size() == 0) {
            return roleService.getRoleByName("maffia");
        }
        if (game.getMaffias().size() == 0) {
            return roleService.getRoleByName("citizen");
        }
        return null;
    }

    private int getAmountReady(Round round) {
        int count = 0;
        for (Player p : round.getAvailablePlayers()) {
            if (p.getState().isReady()) count++;
        }
        return count;
    }

}
