package be.kdg.maffia.login.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static be.kdg.maffia.login.security.WebConstants.HEADER_STRING;
import static be.kdg.maffia.login.security.WebConstants.SECRET;
import static be.kdg.maffia.login.security.WebConstants.TOKEN_PREFIX;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    public JWTAuthorizationFilter(AuthenticationManager authenticationManager){
        super(authenticationManager);
    }

    /**
     * Filters request based on the provided token. When no token is provided, the request will be blocked. When a token is provided, it will be validated.
     * On a valid token, the user will be authenticated and the request will pass.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        UsernamePasswordAuthenticationToken auth = null;
        String header = request.getHeader(HEADER_STRING);
        if(header == null || !header.startsWith(TOKEN_PREFIX)){
            String queryString = request.getQueryString();
            if(queryString != null && queryString.contains("token=")){
                queryString = queryString.replace("token=", "");
                queryString = queryString.replaceFirst("t=.*", "");
                String user = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                        .build()
                        .verify(queryString)
                        .getSubject();
                if(user!=null){
                    auth = new UsernamePasswordAuthenticationToken(user,null,new ArrayList<>());
                }
            } else {
                chain.doFilter(request,response);
                return;
            }
        } else {
            auth = getAuthentication(request);
        }
        SecurityContextHolder.getContext().setAuthentication(auth);
        chain.doFilter(request,response);
    }

    /**
     * Gets token from a HttpRequest Header, and verifies it.
     * @return Valid AuthenticationToken based on the Algorithm that has been selected.
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request){
        String token = request.getHeader(HEADER_STRING);
        if(token!=null){
            String user = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX,""))
                    .getSubject();
            if(user!=null){
                return new UsernamePasswordAuthenticationToken(user,null,new ArrayList<>());
            }
        }
        return null;
    }
}
