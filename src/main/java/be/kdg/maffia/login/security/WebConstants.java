package be.kdg.maffia.login.security;

public class WebConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    //te verwijderen hieronder todo
    public static final String TEMP_CREATE_URL = "/rooms/create";
    public static final String TEMP_GET_ROOMS_URL = "/rooms/getrooms";
}
