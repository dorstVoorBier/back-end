package be.kdg.maffia.login.security;

import be.kdg.maffia.login.model.ApplicationUser;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static be.kdg.maffia.login.security.WebConstants.*;

/**
 * Sets up a JWT Filter which will block any request which does not have a valid JWT-token.
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * Method that catches the AuthenticationRequest. This will try to create Credentials based on the username and password provided by the request.
     * When the Authentication succeeds, it will generate a token and authenticate it.
     * @return Authentication-object with given username and password by Request. Which means the authentication succeeds.
     * @throws AuthenticationException when Authentication does not succeed.
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            ApplicationUser credentials = new ObjectMapper()
                    .readValue(request.getInputStream(), ApplicationUser.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword(), new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e){
            System.out.println(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Creates a token based on the username given by the request. This Token is signed by our Secret key and encrypted by a selected Algorithm.
     * Adds it the the response header.
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException {
        try {
            String token = JWT.create().withSubject(((User) authResult.getPrincipal()).getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                    .sign(Algorithm.HMAC512(SECRET.getBytes()));
            response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
            response.addHeader("Access-Control-Expose-Headers", HEADER_STRING);
        }catch (Exception e){
            throw new IOException("Error while reading request header");
        }
    }
}
