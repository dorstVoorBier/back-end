package be.kdg.maffia.login.exceptions;

public class ApplicationUserRepositoryException extends RuntimeException {
    public ApplicationUserRepositoryException() {
    }

    public ApplicationUserRepositoryException(String message) {
        super(message);
    }
}
