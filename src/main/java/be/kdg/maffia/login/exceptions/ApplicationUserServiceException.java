package be.kdg.maffia.login.exceptions;

public class ApplicationUserServiceException extends RuntimeException {
    public ApplicationUserServiceException(String message){
        super(message);
    }
}
