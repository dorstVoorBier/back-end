package be.kdg.maffia.login.repositories.users;

import be.kdg.maffia.login.model.ApplicationUser;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Repository
public class ApplicationUserRepositoryImpl implements CustomApplicationUserRepository{
    private final MongoTemplate mongoTemplate;

    public ApplicationUserRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Collection<String> findAllUsernames() {
        return getStrings(mongoTemplate);
    }

    public static Collection<String> getStrings(MongoTemplate mongoTemplate) {
        final Query query = new Query();
        Set<String> usernames = new HashSet<>();
        for(ApplicationUser user: mongoTemplate.find(query,ApplicationUser.class)){
            usernames.add(user.getUsername());
        }
        return usernames;
    }

}
