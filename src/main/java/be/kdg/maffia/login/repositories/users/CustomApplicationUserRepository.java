package be.kdg.maffia.login.repositories.users;

import java.util.Collection;

public interface CustomApplicationUserRepository {
    Collection<String> findAllUsernames();
}
