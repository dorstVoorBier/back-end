package be.kdg.maffia.login.repositories.users;

import be.kdg.maffia.login.model.ApplicationUser;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ApplicationUserRepository extends MongoRepository<ApplicationUser, String>,CustomApplicationUserRepository {
    ApplicationUser findByUsername(String username);
    ApplicationUser findByEmail(String email);
    void deleteByUsername(String username);
}
