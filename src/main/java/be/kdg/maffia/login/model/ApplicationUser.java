package be.kdg.maffia.login.model;

import be.kdg.maffia.statistics.model.UserProfile;
import be.kdg.maffia.statistics.model.UserProfileDto;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "applicationusers")
public class ApplicationUser {
    @Id
    private String id;
    private String username;
    private String password;
    private String firstname = "";
    private String lastname = "";
    private String email = "";
    private UserProfile profile;

    public ApplicationUser(){this.profile = new UserProfile();}

    public ApplicationUser(String username, String password, String firstname, String lastname, String email) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.profile= new UserProfile();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ApplicationUserDto toDto(){
        ApplicationUserDto dto = new ApplicationUserDto();
        dto.setFirstname(this.firstname);
        dto.setLastname(this.lastname);
        dto.setEmail(this.email);
        dto.setUsername(this.username);
        UserProfileDto profileDto = new UserProfileDto();
        profileDto.setCitizenGames(this.getProfile().getCitizenGames());
        profileDto.setCitizenWins(this.getProfile().getCitizenWins());
        profileDto.setGamesPlayed(this.getProfile().getGamesPlayed());
        profileDto.setGamesWon(this.getProfile().getGamesWon());
        profileDto.setDoctorGames(this.getProfile().getDoctorGames());
        profileDto.setSherrrifGames(this.getProfile().getSherrrifGames());
        profileDto.setMaffiaGames(this.getProfile().getMaffiaGames());
        profileDto.setMaffiaWins(this.getProfile().getMaffiaWins());
        profileDto.setDoctorProtectCount(this.getProfile().getDoctorProtectCount());
        dto.setProfile(profileDto);
        return dto;
    }
    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }
}

