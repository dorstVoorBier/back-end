package be.kdg.maffia.login.model;


import be.kdg.maffia.statistics.model.UserProfileDto;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ApplicationUserDto {


    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private UserProfileDto profile;


    public ApplicationUserDto(String username, String password, String firstname, String lastname, String email) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserProfileDto getProfile() {
        return profile;
    }

    public void setProfile(UserProfileDto profile) {
        this.profile = profile;
    }
}

