package be.kdg.maffia.login.controller;

import be.kdg.maffia.login.exceptions.ApplicationUserServiceException;
import be.kdg.maffia.login.model.ApplicationUser;
import be.kdg.maffia.login.model.ApplicationUserDto;
import be.kdg.maffia.login.services.ApplicationUserService;
import be.kdg.maffia.statistics.exceptions.ProfileServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Handles User CRUDS, Authentication Requests and Sign-ups.
 */
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {
    private ApplicationUserService appUserService;

    public UserController(ApplicationUserService appUserService) {
        this.appUserService = appUserService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/sign-up")
    public ResponseEntity signUp(@RequestBody ApplicationUser applicationUser) {
        try {
            appUserService.signUp(applicationUser);
            return ResponseEntity.ok(applicationUser.toDto());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.POST,path = "/user/{username}")
    public ResponseEntity getUser(@PathVariable(name = "username")String username){
        try{
            ApplicationUser user = appUserService.getUserByUsername(username);
            return ResponseEntity.ok(user.toDto());
        }catch (ApplicationUserServiceException ause){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ause.getMessage());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Unexpected server error");
        }
    }

    @RequestMapping(method = RequestMethod.DELETE,path ="/delete")
    public ResponseEntity removeUser(@RequestParam(required = true) String username){
        try{
            appUserService.deleteUser(username);
            return ResponseEntity.ok("User deleted successfully");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
    @RequestMapping(method = RequestMethod.PUT,path = "/update")
    public ResponseEntity updateUser(@RequestBody ApplicationUser applicationUser){
        try{
            appUserService.updateUser(applicationUser.getUsername(),applicationUser);
            return ResponseEntity.ok(applicationUser.toDto());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
