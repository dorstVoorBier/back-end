package be.kdg.maffia.login.services;

import be.kdg.maffia.login.exceptions.ApplicationUserServiceException;
import be.kdg.maffia.login.model.ApplicationUser;

public interface ApplicationUserService {
    ApplicationUser signUp(ApplicationUser user);
    ApplicationUser getUserByUsername(String username);
    void deleteUser(String username) throws ApplicationUserServiceException;
    ApplicationUser updateUser(String usernameToUpdate, ApplicationUser userUpdated) throws ApplicationUserServiceException;
}
