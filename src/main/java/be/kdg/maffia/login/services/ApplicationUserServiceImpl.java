package be.kdg.maffia.login.services;

import be.kdg.maffia.login.exceptions.ApplicationUserRepositoryException;
import be.kdg.maffia.login.exceptions.ApplicationUserServiceException;
import be.kdg.maffia.login.exceptions.UsernameNotUniqueException;
import be.kdg.maffia.login.model.ApplicationUser;
import be.kdg.maffia.login.repositories.users.ApplicationUserRepository;
import be.kdg.maffia.login.repositories.users.ApplicationUserRepositoryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class ApplicationUserServiceImpl implements ApplicationUserService {
    private ApplicationUserRepository appUserRepository;
    private ApplicationUserRepositoryImpl customApplicationUserRepository;
    private BCryptPasswordEncoder passwordEncoder;

    public ApplicationUserServiceImpl(ApplicationUserRepository appUserRepository, ApplicationUserRepositoryImpl customApplicationUserRepository, BCryptPasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.customApplicationUserRepository = customApplicationUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public ApplicationUser signUp(ApplicationUser user) throws UsernameNotUniqueException, ApplicationUserServiceException {
        if (isUsernameUnique(user.getUsername())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            appUserRepository.save(user);
            return appUserRepository.findByUsername(user.getUsername());
        } else throw new UsernameNotUniqueException("Username '" + user.getUsername() + "' already taken");
    }

    @Override
    public ApplicationUser getUserByUsername(String username) {
        ApplicationUser user = appUserRepository.findByUsername(username);
        if (user == null) {
            throw new ApplicationUserServiceException("No user found by username: " + username);
        }
        return user;
    }

    @Override
    public ApplicationUser updateUser(String usernameToUpdate, ApplicationUser userUpdated) {
        try {
            ApplicationUser userToUpdate = appUserRepository.findByUsername(usernameToUpdate);
            if (userToUpdate != null) {
                userToUpdate.setPassword(passwordEncoder.encode(userToUpdate.getPassword()));
                return appUserRepository.save(userUpdated);
            }
            return null;
        } catch (Exception e) {
            throw new ApplicationUserServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteUser(String username) throws ApplicationUserServiceException {
        try {
            appUserRepository.deleteByUsername(username);
        } catch (Exception e) {
            throw new ApplicationUserServiceException(e.getMessage());
        }

    }

    private boolean isUsernameUnique(String username) throws ApplicationUserServiceException {
        try {
            Collection<String> users = customApplicationUserRepository.findAllUsernames();
            for (String user : users) {
                if (user.equals(username)) {
                    return false;
                }
            }
            return true;
        } catch (ApplicationUserRepositoryException e) {
            throw new ApplicationUserServiceException(e.getMessage());
        }
    }
}
