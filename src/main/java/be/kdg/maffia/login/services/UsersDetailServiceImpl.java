package be.kdg.maffia.login.services;

import be.kdg.maffia.login.model.ApplicationUser;
import be.kdg.maffia.login.repositories.users.ApplicationUserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UsersDetailServiceImpl implements UserDetailsService {

    private ApplicationUserRepository appUserRepository;

    public UsersDetailServiceImpl(ApplicationUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser user = appUserRepository.findByUsername(username);
        if(user==null){
            throw new UsernameNotFoundException(username);
        }
        return new User(user.getUsername(),user.getPassword(), Collections.emptyList());
    }
}
