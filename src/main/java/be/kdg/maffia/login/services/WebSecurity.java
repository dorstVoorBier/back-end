package be.kdg.maffia.login.services;

import be.kdg.maffia.login.security.JWTAuthenticationFilter;
import be.kdg.maffia.login.security.JWTAuthorizationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

import static be.kdg.maffia.login.security.WebConstants.*;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {
    private UsersDetailServiceImpl usersDetailService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UsersDetailServiceImpl usersDetailService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.usersDetailService = usersDetailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Configures authorization of REST-calls
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*Enable for User Authorization*/
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST,SIGN_UP_URL,TEMP_CREATE_URL, TEMP_GET_ROOMS_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usersDetailService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(Arrays.asList(
                "localhost:3000", "http://localhost:3000", "https://localhost:3000",
                "https://dorstvoorbier.gitlab.io/web-client", "http://dorstvoorbier.gitlab.io/web-client", "dorstvoorbier.gitlab.io/web-client",
                "https://dorstvoorbier.gitlab.io", "http://dorstvoorbier.gitlab.io", "dorstvoorbier.gitlab.io"
        ));
        configuration.applyPermitDefaultValues();
        configuration.addAllowedMethod("DELETE");
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration.applyPermitDefaultValues());
        return source;
    }
}
