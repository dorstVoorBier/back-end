package be.kdg.maffia.chat.repositories;

import be.kdg.maffia.chat.model.ChatRoom;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ChatRepository extends MongoRepository<ChatRoom, Integer> {

    ChatRoom findByRoomId(String roomId);
}
