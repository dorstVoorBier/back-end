package be.kdg.maffia.chat.services;

import be.kdg.maffia.chat.model.ChatMessage;

import java.util.List;

public interface ChatService {
    ChatMessage addMessage(ChatMessage message, String id);
}
