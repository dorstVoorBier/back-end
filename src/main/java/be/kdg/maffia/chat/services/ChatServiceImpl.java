package be.kdg.maffia.chat.services;


import be.kdg.maffia.chat.model.ChatMessage;
import be.kdg.maffia.chat.model.ChatRoom;
import be.kdg.maffia.chat.repositories.ChatRepository;
import org.springframework.stereotype.Service;

@Service
public class ChatServiceImpl implements ChatService{

    private final ChatRepository chatRepository;

    public ChatServiceImpl(ChatRepository chatRepository) {
        this.chatRepository = chatRepository;
    }

    @Override
    public ChatMessage addMessage(ChatMessage message, String id) {

        message.setTime();
        try{
            ChatRoom chatRoom = chatRepository.findByRoomId(id);
            chatRoom.getChat().add(message);
            chatRepository.save(chatRoom);
        } catch (NullPointerException e){
            ChatRoom chatRoom = new ChatRoom();
            chatRoom.setRoomId(id);
            chatRoom.getChat().add(message);
            chatRepository.save(chatRoom);
        }

        return message;
    }
}
