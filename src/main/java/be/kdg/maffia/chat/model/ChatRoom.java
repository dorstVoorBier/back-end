package be.kdg.maffia.chat.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Data
@Document(collection = "chatroom")
public class ChatRoom {

    @Id
    private String roomId;
    private ArrayList<ChatMessage> chat;

    public ChatRoom() {
        this.chat = new ArrayList<>();
    }
}
