package be.kdg.maffia.chat.model;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ChatMessage {

    private String content;
    private String playerName;
    private LocalDateTime localDateTime;

    public ChatMessage(){
    }

    public ChatMessage(String content){
        this.content = content;
        this.playerName = "Game";
        this.localDateTime = LocalDateTime.now();

    }

    public ChatMessage(String content, String playerName) {
        this.content = content;
        this.playerName = playerName;
        this.localDateTime = LocalDateTime.now();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTime(){
        if (localDateTime == null){
            localDateTime = LocalDateTime.now();
        }
    }

}
