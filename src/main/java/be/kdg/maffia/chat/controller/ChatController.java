package be.kdg.maffia.chat.controller;

import be.kdg.maffia.chat.model.ChatMessage;
import be.kdg.maffia.chat.services.ChatServiceImpl;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Controller
public class ChatController {

    private final ChatServiceImpl chatServiceImpl;

    public ChatController(ChatServiceImpl chatServiceImpl) {
        this.chatServiceImpl = chatServiceImpl;
    }

    @MessageMapping("/{id}/allChat")
    @SendTo("/topic/chat/{id}")
    public ChatMessage allChat(@DestinationVariable String id, ChatMessage message) {
        chatServiceImpl.addMessage(message,id);
        return new ChatMessage(message.getContent(), message.getPlayerName());
    }
}
