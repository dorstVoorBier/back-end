package be.kdg.maffia;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.RequestHandledEvent;

/**
 * Prints Events for debugging purposes.
 */
@Component
public class RequestHandler {
    @EventListener
    public void handleEvent (RequestHandledEvent e) {
        System.out.println("-- RequestHandledEvent --");
        System.out.println(e);
    }
}
