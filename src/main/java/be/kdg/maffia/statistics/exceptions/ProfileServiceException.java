package be.kdg.maffia.statistics.exceptions;

public class ProfileServiceException extends RuntimeException {

    public ProfileServiceException() {
    }

    public ProfileServiceException(String message) {
        super(message);
    }

    public ProfileServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
