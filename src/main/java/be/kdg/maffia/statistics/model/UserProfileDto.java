package be.kdg.maffia.statistics.model;

public class UserProfileDto {
    private int gamesPlayed;
    private int gamesWon;
    private int citizenGames;
    private int citizenWins;
    private int maffiaGames;
    private int maffiaWins;
    private int doctorGames;
    private int sherrrifGames;
    private int doctorProtectCount;

    public UserProfileDto(){

    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getCitizenGames() {
        return citizenGames;
    }

    public void setCitizenGames(int citizenGames) {
        this.citizenGames = citizenGames;
    }

    public int getCitizenWins() {
        return citizenWins;
    }

    public void setCitizenWins(int citizenWins) {
        this.citizenWins = citizenWins;
    }

    public int getMaffiaGames() {
        return maffiaGames;
    }

    public void setMaffiaGames(int maffiaGames) {
        this.maffiaGames = maffiaGames;
    }

    public int getMaffiaWins() {
        return maffiaWins;
    }

    public void setMaffiaWins(int maffiaWins) {
        this.maffiaWins = maffiaWins;
    }

    public int getDoctorGames() {
        return doctorGames;
    }

    public void setDoctorGames(int doctorGames) {
        this.doctorGames = doctorGames;
    }

    public int getSherrrifGames() {
        return sherrrifGames;
    }

    public void setSherrrifGames(int sherrrifGames) {
        this.sherrrifGames = sherrrifGames;
    }

    public int getDoctorProtectCount() {
        return doctorProtectCount;
    }

    public void setDoctorProtectCount(int doctorProtectCount) {
        this.doctorProtectCount = doctorProtectCount;
    }
}
