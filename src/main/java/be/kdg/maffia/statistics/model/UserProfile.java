package be.kdg.maffia.statistics.model;

import be.kdg.maffia.login.model.ApplicationUser;

public class UserProfile {
    private int gamesPlayed;
    private int gamesWon;
    private int citizenGames;
    private int citizenWins;
    private int maffiaGames;
    private int maffiaWins;
    private int doctorGames;
    private int sherrrifGames;
    private int doctorProtectCount;

    public UserProfile() {
        this.gamesPlayed=0;
        this.gamesWon=0;
        this.citizenGames=0;
        this.citizenWins=0;
        this.maffiaGames=0;
        this.maffiaWins=0;
        this.doctorGames=0;
        this.sherrrifGames=0;
        this.doctorProtectCount=0;
    }
    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public int getCitizenGames() {
        return citizenGames;
    }

    public int getMaffiaGames() {
        return maffiaGames;
    }

    public int getDoctorProtectCount() {
        return doctorProtectCount;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public void setCitizenGames(int citizenGames) {
        this.citizenGames = citizenGames;
    }

    public int getCitizenWins() {
        return citizenWins;
    }

    public void setCitizenWins(int citizenWins) {
        this.citizenWins = citizenWins;
    }

    public void setMaffiaGames(int maffiaGames) {
        this.maffiaGames = maffiaGames;
    }

    public int getMaffiaWins() {
        return maffiaWins;
    }

    public void setMaffiaWins(int maffiaWins) {
        this.maffiaWins = maffiaWins;
    }

    public int getDoctorGames() {
        return doctorGames;
    }

    public void setDoctorGames(int doctorGames) {
        this.doctorGames = doctorGames;
    }

    public int getSherrrifGames() {
        return sherrrifGames;
    }

    public void setSherrrifGames(int sherrrifGames) {
        this.sherrrifGames = sherrrifGames;
    }

    public void setDoctorProtectCount(int doctorProtectCount) {
        this.doctorProtectCount = doctorProtectCount;
    }

    public void appendGamesPlayed() {
        this.gamesPlayed ++;
    }

    public void appendGamesWon() {
        this.gamesWon++;
    }

    public void appendCitizenGames() {
        this.citizenGames++;
    }
    public void appendCitizenWins(){
        this.citizenWins++;
    }

    public void appendMaffiaWins(){
        this.maffiaWins++;
    }

    public void appendMaffiaGames() {
        this.maffiaGames++;
    }

    public void appendDoctorProtectCount() {
        this.doctorProtectCount++;
    }
    public void appendDoctorGames(){
        this.doctorGames++;
    }
    public void appendSherrifGames(){
        this.sherrrifGames++;
    }


}
