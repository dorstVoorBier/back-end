package be.kdg.maffia.statistics.services.implementations;

import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.login.exceptions.ApplicationUserServiceException;
import be.kdg.maffia.login.model.ApplicationUser;
import be.kdg.maffia.login.repositories.users.ApplicationUserRepository;
import be.kdg.maffia.login.services.ApplicationUserService;
import be.kdg.maffia.statistics.exceptions.ProfileServiceException;
import be.kdg.maffia.statistics.services.interfaces.ProfileService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService {
    private ApplicationUserService applicationUserService;
    private ApplicationUserRepository applicationUserRepository;

    public ProfileServiceImpl(ApplicationUserService applicationUserService, ApplicationUserRepository applicationUserRepository) {
        this.applicationUserService = applicationUserService;
        this.applicationUserRepository = applicationUserRepository;
    }

    @Override
    public void updateGamesPlayed(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendGamesPlayed();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateGamesPlayed(List<Player> playerList) {
        try{
            for(Player p: playerList){
               updateGamesPlayed(p.getUsername());
            }
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }catch (Exception e){
            throw new ProfileServiceException("Unexpected error while updating profile");
        }
    }

    @Override
    public void updateGameWon(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendGamesWon();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateCitizenGames(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendCitizenGames();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateCitizenWins(List<Player> citizens) {
        try{
            for (Player player:citizens){
                ApplicationUser user = applicationUserService.getUserByUsername(player.getUsername());
                user.getProfile().appendCitizenWins();
                applicationUserRepository.save(user);
            }
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateMaffiaGames(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendMaffiaGames();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateMaffiaWins(List<Player> maffias) {
        try{
            for (Player p:maffias){
                ApplicationUser user = applicationUserService.getUserByUsername(p.getUsername());
                user.getProfile().appendMaffiaWins();
                applicationUserRepository.save(user);
            }
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateDoctorProtectCount(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendDoctorProtectCount();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }
    }

    @Override
    public void updateDoctorGames(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendDoctorGames();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }catch (Exception e){
            throw new ProfileServiceException("Unexpected error while updating Doctor statistic in profli",e.getCause());
        }
    }

    @Override
    public void updateSherrifGames(String username) {
        try{
            ApplicationUser user = applicationUserService.getUserByUsername(username);
            user.getProfile().appendSherrifGames();
            applicationUserRepository.save(user);
        }catch (ApplicationUserServiceException ause){
            throw new ProfileServiceException(ause.getMessage(),ause.getCause());
        }catch (Exception e){
            throw new ProfileServiceException("Unexpected error while updating Sherrif statistic in profile.",e.getCause());
        }
    }
}
