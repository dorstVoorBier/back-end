package be.kdg.maffia.statistics.services.interfaces;

import be.kdg.maffia.game.model.Player;
import be.kdg.maffia.login.model.ApplicationUser;

import java.util.List;

public interface ProfileService {
    void updateGamesPlayed(List<Player> playerList);
    void updateGamesPlayed(String username);
    void updateGameWon(String username);
    void updateCitizenGames(String username);
    void updateCitizenWins(List<Player> citizens);
    void updateMaffiaWins(List<Player> maffias);
    void updateMaffiaGames(String username);
    void updateDoctorProtectCount(String username);
    void updateDoctorGames(String username);
    void updateSherrifGames(String username);
}
