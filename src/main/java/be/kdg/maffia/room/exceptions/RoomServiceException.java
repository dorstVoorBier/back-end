package be.kdg.maffia.room.exceptions;

public class RoomServiceException extends RuntimeException {
    public RoomServiceException() {
    }

    public RoomServiceException(String message) {
        super(message);
    }

    public RoomServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
