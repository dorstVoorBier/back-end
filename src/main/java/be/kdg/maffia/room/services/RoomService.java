package be.kdg.maffia.room.services;

import be.kdg.maffia.room.model.Room;

import java.util.List;

public interface RoomService {
    Room save(Room room);
    List<Room> getRoomsWithPlayers();
    List<Room> getRoomsWithPlayersAsc();
    List<Room> getRoomsWithPlayersDesc();
    Room join(String id,String username);
    Room updateSetting(String id,String setting, String value) throws NumberFormatException;
    Room leave(String id,String username);
    Room getRoom(String id);
    Boolean doesRoomHavePassword(String id);

}
