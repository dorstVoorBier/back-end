package be.kdg.maffia.room.services;

import be.kdg.maffia.room.exceptions.RoomServiceException;
import be.kdg.maffia.room.model.Room;
import be.kdg.maffia.room.repositories.RoomRepository;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class RoomServiceImpl implements RoomService {

    private final RoomRepository repo;

    public RoomServiceImpl(RoomRepository repo) {
        this.repo = repo;
    }

    public Room save(Room room) {
        return repo.save(room);
    }

    @Override
    public List<Room> getRoomsWithPlayers() {
        List<Room> rooms = repo.findAll();
        return filterRoomsWithPlayers(rooms);
    }

    @Override
    public List<Room> getRoomsWithPlayersAsc() {
        List<Room> roomsFiltered = filterRoomsWithPlayers(repo.findAll());
        return roomsFiltered.stream().sorted((room1, room2) -> room1.getRoomName().compareToIgnoreCase(room2.getRoomName())).collect(Collectors.toList());
    }

    @Override
    public List<Room> getRoomsWithPlayersDesc() {
        List<Room> roomsFiltered = filterRoomsWithPlayers(repo.findAll());
        return roomsFiltered.stream().sorted((room1, room2) -> room2.getRoomName().compareToIgnoreCase(room1.getRoomName())).collect(Collectors.toList());
    }

    public Room join(String roomId, String username) throws RoomServiceException {
        try {
            Boolean playerExists = false;
            Room room = repo.findRoomById(roomId);

            if (room.getFull()) {
                System.out.println("the room is full");
                return repo.save(room);
            }
            List players = room.getPlayers();
            for (Object player : players) {
                if (player.equals(username)) {
                    playerExists = true;
                }
            }
            if (!playerExists) {
                room.addPlayer(username);
                room = makeRoomFullorOpen(room);
            }
            return repo.save(room);
        } catch (Exception e) {
            throw new RoomServiceException("Unexpected error while trying to join room: " + e.getMessage(), e.getCause());
        }

    }

    @Override
    public Room updateSetting(String id, String setting, String value) throws NumberFormatException, RoomServiceException {
        try {
            Room room = repo.findRoomById(id);
            if (room == null) {
                throw new RoomServiceException("Could not find room by ID: " + id);
            }
            switch (setting) {
                case "max":
                    if ("".equals(value)) {
                        room.setMaxSpelers(0);
                    } else {
                        room.setMaxSpelers(Integer.parseInt(value));
                    }
                    room = makeRoomFullorOpen(room);
                    break;
                case "min":
                    if ("".equals(value)) {
                        room.setMinSpelers(0);
                    } else {
                        room.setMinSpelers(Integer.parseInt(value));
                    }
                    break;
                case "day":
                    if ("".equals(value)) {
                        room.setSecDayRoundTime(0);
                    } else {
                        room.setSecDayRoundTime(Integer.parseInt(value));
                    }
                    break;
                case "night":
                    if ("".equals(value)) {
                        room.setSecNightRoundTime(0);
                    } else {
                        room.setSecNightRoundTime(Integer.parseInt(value));
                    }
                    break;
            }
            repo.save(room);
            return room;
        } catch (Exception e) {
            throw new RoomServiceException("Unexpected error while updating settings.", e.getCause());
        }
    }

    @Override
    public Room leave(String roomId, String username) {
        try {
            Room room = repo.findRoomById(roomId);
            if (room == null) throw new RoomServiceException("No room found for ID:" + roomId);
            //When the host leaves the room, the room will be deleted
            if (room.getHost().equals(username)) {
                room.setPlayers(new ArrayList<>());
                repo.delete(room);
                return room;
            } else {
                room.removePlayer(username);
                //When there are no more players, the room will be deleted.
                if (room.getPlayers().size() == 0) {
                    repo.delete(room);
                    return room;
                } else {
                    room.removePlayer(username);
                    //When there are no players in the room, the room will be deleted
                    if (room.getPlayers().size() == 0) {
                        repo.delete(room);
                        return room;
                    }
                }
            }
            repo.save(room);
            return room;
        } catch (Exception e) {
            throw new RoomServiceException("Unexpected error while leaving room:" + roomId, e.getCause());
        }
    }

    @Override
    public Room getRoom(String id) {
        return repo.findRoomById(id);
    }

    @Override
    public Boolean doesRoomHavePassword(String id) {
        Room room = repo.findRoomById(id);
        return !room.getPassword().equals("");

    }

    public Room makeRoomFullorOpen(Room room) {
        if (room.getPlayers().size() == room.getMaxSpelers()) {
            room.setFull(true);
        } else room.setFull(false);

        return room;
    }

    private List<Room> filterRoomsWithPlayers(List<Room> rooms) {
        List<Room> roomsFiltered = new ArrayList<>();
        for (Room room : rooms) {
            if (!room.getFull() && !room.getClosed()) {
                roomsFiltered.add(room);
            }
        }
        return roomsFiltered;
    }
}
