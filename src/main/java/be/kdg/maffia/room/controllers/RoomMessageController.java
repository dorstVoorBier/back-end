package be.kdg.maffia.room.controllers;

import be.kdg.maffia.room.exceptions.RoomServiceException;
import be.kdg.maffia.room.model.Room;
import be.kdg.maffia.room.model.dto.RoomDTO;
import be.kdg.maffia.room.model.messages.RoomJoinMessage;
import be.kdg.maffia.room.model.messages.RoomSettingMessage;
import be.kdg.maffia.room.services.RoomService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;


@Controller
public class RoomMessageController {
    final
    RoomService roomService;
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public RoomMessageController(RoomService roomService) {
        this.roomService = roomService;
    }


    @MessageMapping("/{roomId}/join")
    @SendTo("/topic/{roomId}/roominfo")
    public ResponseEntity join(@DestinationVariable String roomId, RoomJoinMessage message) {
        try {
            Room room = roomService.join(roomId, message.getText());
            return new ResponseEntity<>(modelMapper.map(room, RoomDTO.class), HttpStatus.OK);
        } catch (RoomServiceException rse) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rse.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("unexpected Error");
        }
    }


    @MessageMapping("/{roomId}/settingchange")
    @SendTo("/topic/{roomId}/roominfo")
    public ResponseEntity changeInSettings(@DestinationVariable String roomId, RoomSettingMessage message) {
        try {
            Room room = roomService.updateSetting(roomId, message.getSetting(), message.getValue());
            return new ResponseEntity<>(modelMapper.map(room, RoomDTO.class), HttpStatus.OK);
        } catch (NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Invalid Number Format"+e.getMessage());
        } catch (RoomServiceException rse) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(rse.getMessage());
        }
    }

    @MessageMapping("/{roomId}/leave")
    @SendTo("/topic/{roomId}/roominfo")
    public ResponseEntity leave(@DestinationVariable String roomId, RoomJoinMessage message) throws InterruptedException {
        try {
            Room room = roomService.leave(roomId, message.getText());
            System.out.println("Left the room: " + message.getText());
            return new ResponseEntity<>(modelMapper.map(room, RoomDTO.class), HttpStatus.OK);
        } catch (RoomServiceException rse){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(rse.getMessage());
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Unexpected Error");
        }
    }

}
