package be.kdg.maffia.room.controllers;

import be.kdg.maffia.room.exceptions.RoomServiceException;
import be.kdg.maffia.room.model.Room;
import be.kdg.maffia.room.model.dto.RoomDTO;
import be.kdg.maffia.room.model.dto.RoomInfoDTO;
import be.kdg.maffia.room.services.RoomService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
@RequestMapping("/rooms")
public class RoomWebController {
   private ModelMapper modelMapper = new ModelMapper();
    @Autowired
    RoomService service;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<RoomDTO> create(@RequestBody Room room)  {
        System.out.println("create " + room.getRoomName());
        Room roomout = service.save(room);
        return new ResponseEntity<>(modelMapper.map(roomout, RoomDTO.class), HttpStatus.OK);
    }

    @RequestMapping(value = "/getrooms", method = RequestMethod.GET)
    public ResponseEntity getRooms() {
        try {
            List roominfos = service.getRoomsWithPlayers().stream().map(room -> new RoomInfoDTO(room.getId(),room.getRoomName(),room.getPlayers().size(),room.getMaxSpelers())).collect(Collectors.toList());
            return new ResponseEntity<>(roominfos, HttpStatus.OK);
        } catch (RoomServiceException rse) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(rse.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Unexpected server error. " + e.getMessage());
        }
    }

    @RequestMapping(value = "/getrooms/{order}", method = RequestMethod.GET)
    public ResponseEntity<Object> getRoomsOrdered(@PathVariable(name = "order") String order) {
        try {
            List<RoomInfoDTO> roominfos;
            switch (order) {
                case "asc":
                    roominfos = service.getRoomsWithPlayersAsc().stream().map(room -> new RoomInfoDTO(room.getId(), room.getRoomName(), room.getPlayers().size(), room.getMaxSpelers())).collect(Collectors.toList());
                    break;
                case "desc":
                    roominfos = service.getRoomsWithPlayersDesc().stream().map(room -> new RoomInfoDTO(room.getId(), room.getRoomName(), room.getPlayers().size(), room.getMaxSpelers())).collect(Collectors.toList());
                    break;
                default:
                    roominfos = service.getRoomsWithPlayers().stream().map(room -> new RoomInfoDTO(room.getId(), room.getRoomName(), room.getPlayers().size(), room.getMaxSpelers())).collect(Collectors.toList());
                    break;
            }
            return new ResponseEntity<>(roominfos, HttpStatus.OK);
        } catch (RoomServiceException rse) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(rse.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Unexpected server exception. " + e.getMessage());
        }
    }

    @RequestMapping(value = "/haspassword", method = RequestMethod.GET)
    public ResponseEntity<Boolean> hasPassword(@RequestBody String roomid){
        Boolean pw = service.doesRoomHavePassword(roomid);
        return new ResponseEntity<>(pw,HttpStatus.OK);
    }

    @RequestMapping(value = "/playerdisconnects", method = RequestMethod.POST)
    public ResponseEntity<Room> playerdisconnects(@RequestParam("roomid") String roomid,@RequestParam("username") String username){
        System.out.println("dc");
        Room room =service.leave(roomid,username);
        return new ResponseEntity<>(room,HttpStatus.OK);
    }

}
