package be.kdg.maffia.room.repositories;

import be.kdg.maffia.room.model.Room;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface RoomRepository extends MongoRepository<Room,Integer> {
    Room findRoomById(String id);
}
