package be.kdg.maffia.room.model.dto;

import lombok.Data;

@Data
public class RoomInfoDTO {
    String roomId;
    String roomName;
    int aantalSpelers;
    int maxAantalSpelers;
    String password;
    public RoomInfoDTO() {
    }

    public RoomInfoDTO(String roomId,String roomName,int aantalSpelers,int maxAantalSpelers) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.aantalSpelers = aantalSpelers;
        this.maxAantalSpelers = maxAantalSpelers;
    }
}
