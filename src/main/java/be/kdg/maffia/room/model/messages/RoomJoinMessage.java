package be.kdg.maffia.room.model.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.socket.WebSocketMessage;

@Data
@AllArgsConstructor
public class RoomJoinMessage implements WebSocketMessage {

    private String text;

    @Override
    public Object getPayload() {
        return text;
    }

    @Override
    public int getPayloadLength() {
        return text.length();
    }

    @Override
    public boolean isLast() {
        return false;
    }
}
