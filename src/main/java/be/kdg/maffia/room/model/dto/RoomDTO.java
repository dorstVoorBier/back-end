package be.kdg.maffia.room.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class RoomDTO {
    public String id;
    public String roomName;
    public int maxSpelers;
    public int minSpelers;
    public List players;
    public int secDayRoundTime;
    public int secNightRoundTime;
    public String password;
    public String host;
    public Boolean closed;
    public Boolean full;

}
