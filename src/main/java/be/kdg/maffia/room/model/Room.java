package be.kdg.maffia.room.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "rooms")
public class Room {
    @Id
    String id;

    String roomName;
    int maxSpelers;
    int minSpelers;
    List<String> players;
    int secDayRoundTime;
    int secNightRoundTime;
    Boolean closed;
    Boolean full;
    String password;
    String host;

    public Room() {
        players = new ArrayList<>();
    }

    public Room(String roomName, int maxSpelers, int minSpelers, ArrayList<String> players, int secDayRoundTime, int secNightRoundTime, Boolean closed, Boolean full, String password, String host) {
        this.roomName = roomName;
        this.maxSpelers = maxSpelers;
        this.minSpelers = minSpelers;
        this.players = players;
        this.secDayRoundTime = secDayRoundTime;
        this.secNightRoundTime = secNightRoundTime;
        this.closed = closed;
        this.full = full;
        this.password = password;
        this.host = host;
    }

    public void addPlayer(String username){
        System.out.println("add player" + username);
        players.add(username);
    }

    public void removePlayer(String username){
        players.remove(username);
    }
}