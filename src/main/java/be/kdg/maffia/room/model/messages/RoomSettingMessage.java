package be.kdg.maffia.room.model.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.socket.WebSocketMessage;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomSettingMessage implements WebSocketMessage {

    String setting;
    String value;

    @Override
    public Object getPayload() {
        return null;
    }

    @Override
    public int getPayloadLength() {
        return 0;
    }

    @Override
    public boolean isLast() {
        return false;
    }
}
