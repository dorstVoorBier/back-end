package be.kdg.maffia.rules.services.exceptions;

public class InfoServiceException extends Exception {

    public InfoServiceException(String message) {
        super(message);
    }

    public InfoServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
