package be.kdg.maffia.rules.services.implementations;

import be.kdg.maffia.game.model.playerroles.PlayerRole;
import be.kdg.maffia.game.services.interfaces.PlayerRoleService;
import be.kdg.maffia.rules.model.InfoFile;
import be.kdg.maffia.rules.repositories.interfaces.InfoRepository;
import be.kdg.maffia.rules.services.exceptions.InfoServiceException;
import be.kdg.maffia.rules.services.exceptions.InvalidRoleException;
import be.kdg.maffia.rules.services.interfaces.InfoService;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.sound.sampled.Line;
import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InfoServiceImpl implements InfoService {
    private InfoRepository infoRepository;
    private PlayerRoleService roleService;

    @Value("${rules.filename}")
    private String filenameRules;
    @Value("${citizen.filename}")
    private String filenameCitizen;
    @Value("${doctor.filename}")
    private String filenameDoctor;
    @Value("${maffia.filename}")
    private String filenameMaffia;
    @Value("${sherrif.filename}")
    private String filenameSherrif;


    public InfoServiceImpl(InfoRepository infoRepository, PlayerRoleService roleService) {
        this.infoRepository = infoRepository;
        this.roleService = roleService;
    }

    @Override
    public InfoFile getRules() throws InfoServiceException {
        try {
            InfoFile ruleFile = infoRepository.getByFilename(filenameRules);
            if (ruleFile == null) {
                ruleFile = createInfoFile(filenameRules);
            }
            return ruleFile;
        } catch (Exception e) {
            throw new InfoServiceException("Error while finding Rules-file.", e.getCause());
        }

    }

    @Override
    public InfoFile getRoleInfo(String role) throws InfoServiceException, InvalidRoleException {
        try {
            InfoFile roleFile;
            String filename;
            switch (role) {
                case "citizen":
                    filename = filenameCitizen;
                    break;
                case "maffia":
                    filename = filenameMaffia;
                    break;
                case "doctor":
                    filename = filenameDoctor;
                    break;
                case "sherrif":
                    filename = filenameSherrif;
                    break;
                default:
                    throw new InvalidRoleException("No valid role give, could not fetch RoleInfo");
            }
            roleFile = infoRepository.getByFilename(filename);
            if (roleFile == null) roleFile = createInfoFile(filename);
            return roleFile;
        }catch (InvalidRoleException ire){
            throw ire;
        } catch (Exception e) {
            throw new InfoServiceException("Error while finding InfoFile for role: " + role, e.getCause());
        }
    }

    private InfoFile createInfoFile(String filename) throws InfoServiceException {
        InfoFile ruleFile;
        try {
            File file = ResourceUtils.getFile("classpath:info/" + filename);
            byte[] data = IOUtils.toByteArray(new FileInputStream(file));
            ruleFile = new InfoFile(filename, new String(data, Charset.defaultCharset()), file.length());
            infoRepository.save(ruleFile);
        } catch (Exception e) {
            throw new InfoServiceException("Unexpected Error while reading RuleFile");
        }
        return ruleFile;
    }
}
