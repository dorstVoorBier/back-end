package be.kdg.maffia.rules.services.exceptions;

public class InvalidRoleException extends RuntimeException {
    public InvalidRoleException() {
    }

    public InvalidRoleException(String message) {
        super(message);
    }

    public InvalidRoleException(String message, Throwable cause) {
        super(message, cause);
    }
}
