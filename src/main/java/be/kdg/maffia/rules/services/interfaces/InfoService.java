package be.kdg.maffia.rules.services.interfaces;

import be.kdg.maffia.rules.model.InfoFile;
import be.kdg.maffia.rules.services.exceptions.InfoServiceException;

public interface InfoService {
    InfoFile getRules() throws InfoServiceException;
    InfoFile getRoleInfo(String role) throws InfoServiceException;
}
