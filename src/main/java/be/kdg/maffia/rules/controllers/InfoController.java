package be.kdg.maffia.rules.controllers;

import be.kdg.maffia.rules.services.exceptions.InfoServiceException;
import be.kdg.maffia.rules.services.interfaces.InfoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

    private ObjectMapper mapper = new ObjectMapper();
    private InfoService infoService;

    public InfoController(InfoService infoService) {
        this.infoService = infoService;
    }

    @RequestMapping(method = RequestMethod.GET,value = "rules")
    public ResponseEntity getRules(){
        try{
            return ResponseEntity.ok(infoService.getRules());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET,value = "roleinfo/{role}")
    public ResponseEntity getInfoFile(@PathVariable(name = "role")String role){
        try{
            return ResponseEntity.ok(mapper.writeValueAsString(infoService.getRoleInfo(role)));
        } catch (InfoServiceException | JsonProcessingException e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }

    }
}
