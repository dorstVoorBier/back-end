package be.kdg.maffia.rules.repositories.interfaces;

import be.kdg.maffia.rules.model.InfoFile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoRepository extends MongoRepository<InfoFile,String> {
    InfoFile getById(String id);
    InfoFile getByFilename(String filename);
}
