package be.kdg.maffia.rules.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("infofile")
public class InfoFile {
    String id;
    String filename;
    String content;
    long fileLength;

    public InfoFile(String filename, String content, long fileLength) {
        this.filename = filename;
        this.content = content;
        this.fileLength = fileLength;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getData() {
        return content;
    }

    public void setData(String content) {
        this.content = content;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }
}
